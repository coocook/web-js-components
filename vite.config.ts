import { defineConfig, mergeConfig, UserConfig } from "vite";
import react from "@vitejs/plugin-react";
import { resolve } from "path";

const commonConfig = defineConfig({
    server: {
        open: true,
    },
    build: {
        target: "esnext",
        emptyOutDir: false,
    },
    plugins: [react()],
});

const projects: Record<string, UserConfig> = {
    autocomplete: {
        root: resolve(__dirname, "src/apps/autocomplete"),
        build: {
            emptyOutDir: true,
            rollupOptions: {
                output: {
                    dir: "dist/autocomplete",
                    entryFileNames: "autocomplete.[format].js",
                    chunkFileNames: "[name].[format].js",
                    assetFileNames: "autocomplete_[name][extname]",
                },
            },
        },
    },
    "ingredients-editor": {
        root: resolve(__dirname, "src/apps/ingredients-editor"),
        build: {
            emptyOutDir: true,
            rollupOptions: {
                output: {
                    dir: "dist/ingredients-editor",
                    entryFileNames: "ingredients-editor.[format].js",
                    chunkFileNames: "[name].[format].js",
                    assetFileNames: "ingredients-editor_[name][extname]",
                },
            },
        },
    },
    "meals-dishes-editor": {
        root: resolve(__dirname, "src/apps/meals-dishes-editor"),
        build: {
            emptyOutDir: true,
            rollupOptions: {
                output: {
                    dir: "dist/meals-dishes-editor",
                    entryFileNames: "meals-dishes-editor.[format].js",
                    chunkFileNames: "[name].[format].js",
                    assetFileNames: "meals-dishes-editor_[name][extname]",
                },
            },
        },
    },
};

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
    if (command === "serve") {
        return commonConfig;
    }

    const reactStagingConfig = /^.*-staging$/.test(mode)
        ? defineConfig({
              plugins: [react()],
          })
        : {};

    switch (mode) {
        case "meals-dishes-editor-staging":
            return mergeConfigList([
                commonConfig,
                reactStagingConfig,
                projects["meals-dishes-editor"],
                {
                    build: {
                        minify: false,
                    },
                },
            ]);
            {
            }
            break;
        case "meals-dishes-editor-production":
            return mergeConfigList([
                commonConfig,
                projects["meals-dishes-editor"],
                defineConfig({
                    define: {
                        "process.env.NODE_ENV": JSON.stringify("production"),
                    },
                }),
            ]);
            break;
        case "ingredients-editor-staging":
            return mergeConfigList([
                commonConfig,
                reactStagingConfig,
                projects["ingredients-editor"],
                {
                    build: {
                        minify: false,
                    },
                },
            ]);
            {
            }
            break;
        case "ingredients-editor-production":
            return mergeConfigList([
                commonConfig,
                projects["ingredients-editor"],
                defineConfig({
                    define: {
                        "process.env.NODE_ENV": JSON.stringify("production"),
                    },
                }),
            ]);
            break;
        case "autocomplete-staging":
            return mergeConfigList([
                commonConfig,
                reactStagingConfig,
                projects.autocomplete,
                {
                    build: {
                        minify: false,
                    },
                },
            ]);
            break;
        case "autocomplete-production":
            return mergeConfigList([
                commonConfig,
                projects.autocomplete,
                defineConfig({
                    define: {
                        "process.env.NODE_ENV": JSON.stringify("production"),
                    },
                }),
            ]);
            break;
    }
});

function mergeConfigList(
    configs: Record<string, any>[],
    isRoot?: boolean
): Record<string, any> {
    let merged = configs[0];
    if (configs.length <= 1) return merged;
    for (let idx = 1; idx < configs.length; idx++) {
        merged = mergeConfig(merged, configs[idx], isRoot);
    }

    return merged;
}
