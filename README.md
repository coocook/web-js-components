# Coocook Web Components

Subproject of [Coocook](https://gitlab.com/coocook/coocook)

## Requirements
- [`pnpm`](https://pnpm.io/installation) for managing Javascript dependencies

## Development
For development of this repository you need to have both this repository and the
main Coocook repository on your computer. You can acquire them with the following
two commands:
```console
$ git clone https://gitlab.com/coocook/web-components.git
$ git clone https://gitlab.com/coocook/coocook.git
```

To build the web components and copy them into the Coocook directory tree run following command inside the web components directory:
```console
$ cd "web-components/"
$ pnpm i
$ pnpm run dev "../coocook/"
```

Please consult the [Coocook Readme](https://gitlab.com/coocook/coocook/-/blob/master/README.md) to learn how to run the Coocook app.
