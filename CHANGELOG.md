# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [0.7.2] - 2025-03-02

### Fixed
- redeclaration of const through import of `autocomplete.css` in web compenents and autocomplete react component

## [0.7.1] - 2025-03-01

### Added
- simple form validation for `autocomplete.tsx`

### Changed
- only send update requests after user has finished typing in inputs in `IngredientsEditor.tsx`

### Fixed
- keyboard navigation and form submission in `autocomplete.tsx`

## [0.7.0] - 2025-03-01

### Changed
- renamed cc-search to cc-ajax-autocomplete

## [0.6.5] - 2024-10-14

### Changed
- ~renamed cc-search to cc-ajax-autocomplete~

### Fixed
- no longer mark search matches in option html markup

## [0.6.4] - 2024-10-24

### Fixed
- varius issues with cc-autocomplete and cc-multi-autocomplete (#65, #66, #67, #68, #77)

## [0.6.3] - 2024-09-09

### Changed
- cc-multi-autocomplete: pass value as multivalued data to backend
- cc-multi-autocomplete: accept JSON data as initial value

## [0.6.2] - 2024-08-08

### Changed
- cc-multi-autocomplete: improve internal value handling
- Limit height of react autocomplete select option box
- Remove hard option limit in react autocomplete component

## [0.6.1] - 2024-06-26

### Added
- web components: add cross to multi autocomplete deselect buttons

## [0.6.0] - 2024-06-03

### Added
- web components: cc-multi-autocomplete, for selecting multiple elements with autocomplete
- web components: keyboard accessibility for components

## [0.5.1] - 2024-06-02

### Fixed
- web components: renamed component classes due to name conflict with the editors

## [0.5.0] - 2024-06-01

### Added
- web components: split cc-autocomplete component in cc-autocomplete (only from predefined list) and cc-search (fetch completion list from server)

### Changed
- meals-and-dishes editor: dish row element is now an anchor
- ingredients editor: better suspense messages
- web components: rewrite in typescript
- web components: allow bootstrap styles and icons inside of components

### Fixed
- Incorrect terminology amount to value

## [0.4.0] - 2023-12-20

### Added
-   Highlighting of preferred units

### Changed
-   Disallow negative amounts for ingredients
-   Swap prepared and not prepared ingredients block in ingredients editor

### Fixed
-   Handling of duplicate Meal names on same date when moving or updating a Meal

## [0.3.2] - 2023-07-19

### Fixed
-   Bug where user was able to create or change a Meal with an empty string as name
-   invalid POST URL for updating a meal

## [0.3.1] - 2023-07-10

### Added
-   UI change for highlighting that a Meal or Dish is currently dragged

### Fixed
-   Cancellation of drag and drop in <MealsDishesEditor /> does not revert changes made
-   Dragging dish into meal below inserts it at the end not at the top like one would expect

## [0.3.0] - 2023-07-08

## [0.2.0] - 2022-02-18

### Added
-   Automatic builds and releases via Gitlab CI

### Changed
-   Update pnpm dependencies
    -   vite@3 -> vite@4
    -   @vitejs/plugin-react@2 -> @vitejs/plugin-react@3

### Fixed
-   Bug where vite produces 2 versions of React in the build output
