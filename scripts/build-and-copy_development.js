#!/usr/bin/env node

const { cpSync, rmdirSync, renameSync, mkdirSync, rmSync } = require('node:fs');
const path = require('node:path');
const { argv, exit } = require('node:process');
const { spawnSync } = require('node:child_process');

if (require.main === module) {
    onError(async () => {
        const { default: chalk } = await import('chalk');

        const { webjscomponentsRoot, targetDir } = onError(() => {
            const webjscomponentsRoot = path.resolve(`${__dirname}/..`);
            const targetDir = path.join(
                path.resolve(argv[2]),
                'root/static/lib/coocook-web-components/dist'
            );

            return {
                webjscomponentsRoot,
                targetDir,
            };
        }, () => printAndExit(chalk.red("Please provide the location of your 'coocook' directory!")));

        await run(webjscomponentsRoot, targetDir, chalk);
    }, printAndExit);
}

async function run(webjscomponentsRoot, targetDir, chalk) {
    spawnSync('npm', ['run', 'build:staging'], { stdio: 'inherit' });

    console.log(chalk.yellow(`- Deleting '${targetDir}'...`));
    ignoreError(() => rmSync(targetDir, { recursive: true }));
    ignoreError(() => rmSync(path.resolve(targetDir, '..', 'dist'), { recursive: true }))
    console.log(chalk.green(`- Deleted '${targetDir}' successfully`));

    console.log(chalk.yellow(`- Copying from '${path.join(webjscomponentsRoot, 'dist')}' to '${targetDir}'...`));
    onError(() => {
        mkdirSync(path.resolve(targetDir, '..', 'dist'), { recursive: true });
        cpSync(
            path.join(webjscomponentsRoot, 'dist/'),
            path.resolve(targetDir, '..', 'dist'),
            { recursive: true });
        renameSync(
            path.resolve(targetDir, '..', 'dist'),
            path.resolve(targetDir));
    }, printAndExit);
    console.log(chalk.green(`- Copied from '${path.join(webjscomponentsRoot, 'dist')}' to '${targetDir}' successfully`));
}

async function ignoreError(fn) {
    try {
        return fn();
    } catch (_) {
        //ignore
        return null;
    }
}

function onError(fn, errFn) {
    try {
        return fn();
    } catch (err) {
        return errFn(err);
    }
}

function printAndExit(printable, exitCode = 1) {
    console.error(printable);
    exit(exitCode);
}

module.exports = run;