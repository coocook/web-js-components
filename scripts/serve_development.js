#!/usr/bin/env node

const watch = require("node-watch");
const path = require("node:path");
const { argv, exit } = require("node:process");
const build = require("./build-and-copy_development");
const { createInterface } = require("node:readline");
const { stdin: input, stdout: output } = require("node:process");

const rl = createInterface({ input, output });

onError(async () => {
    const { default: chalk } = await import("chalk");

    const { webjscomponentsRoot, targetDir } = onError(
        () => {
            const webjscomponentsRoot = path.resolve(`${__dirname}/..`);
            const targetDir = path.join(
                path.resolve(argv[2]),
                "root/static/lib/coocook-web-components/dist"
            );

            return {
                webjscomponentsRoot,
                targetDir,
            };
        },
        () =>
            printAndExit(
                chalk.red(
                    "Please provide the location of your 'coocook' directory!"
                )
            )
    );

    function buildAndCopy() {
        onError(
            () => {
                console.log(
                    chalk.yellow("Building 'coocook-web-components'...")
                );
                build(webjscomponentsRoot, targetDir, chalk);
                console.log();
                console.log(
                    chalk.green("=========================================")
                );
                console.log(
                    chalk.green("Built 'coocook-web-components' successfully.")
                );
                console.log(
                    chalk.green("=========================================")
                );
            },
            (err) => console.error(err)
        );
    }

    buildAndCopy();

    console.log();

    console.log(
        chalk.yellow(
            `Watching '${path.resolve(webjscomponentsRoot, "src")}'...`
        )
    );
    rl.on("line", (input) => {
        if (input === "rs") buildAndCopy();
    });
    watch(
        path.resolve(webjscomponentsRoot, "src"),
        { recursive: true },
        debounce(buildAndCopy, 1000)
    );
}, printAndExit);

function onError(fn, errFn) {
    try {
        return fn();
    } catch (err) {
        return errFn(err);
    }
}

function printAndExit(printable, exitCode = 1) {
    console.error(printable);
    rl.close();
    exit(exitCode);
}

function debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}