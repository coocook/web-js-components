import Fuse from "fuse.js";
import Immutable from "immutable";
import { nanoid } from "nanoid";
import React, { ReactNode } from "react";
import { c } from "../util/conditional-css-classes";

import "./autocomplete.css";

export type AutocompleteOptionID = string;

export type AutocompleteOption<T> = {
    value: AutocompleteOptionID;
    label: string;
    option: T;
    markIcon?: string;
    matches?: readonly Fuse.FuseResultMatch[];
};

export type AutocompleteProps<T> = {
    id?: string;
    ref?: React.MutableRefObject<HTMLInputElement | null>;
    className?: string;
    size?: "sm" | "lg";
    style?: React.CSSProperties;
    isInvalid?: boolean;
    invalidMessage?: string;
    label?: string | JSX.Element;
    floatingLabel?: boolean;
    value?: AutocompleteOptionID;
    defaultValue?: AutocompleteOptionID;
    options: AutocompleteOption<T>[];
    onChange?: (selectedOption: AutocompleteOption<T> | string) => void;
    onBlur?: (selectedOption: AutocompleteOption<T> | string) => void;
};

export type AutocompleteSelectOptionProps = {
    label: ReactNode;
    hovered: boolean;
    icon?: string;
    onClick: () => void;
};

type ReceiverID = string;
type ShouldGetFocusEvent = {
    id: string;
};
type Receiver = {
    receive: () => ShouldGetFocusEvent[];
};

let receivers = Immutable.Map<
    ReceiverID,
    Immutable.List<ShouldGetFocusEvent>
>();
export const click = (id: ReceiverID) => {
    receivers = receivers.set(
        id,
        (receivers.get(id) ?? Immutable.List()).push({ id: nanoid(5) }),
    );
};
const connect = (id: ReceiverID) => {
    receivers = receivers.set(id, Immutable.List());
    return {
        receive: () => {
            const events = receivers.get(id);
            receivers = receivers.set(id, Immutable.List());
            return events?.toArray() ?? [];
        },
    };
};
const disconnect = (id: ReceiverID) => {
    receivers = receivers.delete(id);
};

export const Autocomplete = <TOption,>(props: AutocompleteProps<TOption>) => {
    const inputRef = React.useRef<HTMLInputElement | null>(null);
    const container = React.useRef<HTMLDivElement | null>(null);

    const id = React.useRef(props.id ?? nanoid());
    const inputID = `input-autocomplete-${id.current}`;
    const labelID = `label-autocomplete-${id.current}`;
    const [focusReceiver, setFocusReceiver] = React.useState<Receiver | null>(
        null,
    );

    /* Dropdown search */
    const fuse = React.useMemo(
        () =>
            new Fuse(props.options, {
                keys: ["label"],
                includeMatches: true,
                sortFn: (a, b) => {
                    if (
                        props.options[a.idx].markIcon ===
                        props.options[b.idx].markIcon
                    ) {
                        return a.score - b.score;
                    }

                    return props.options[a.idx].markIcon ? -1 : 1;
                },
            }),
        [props.options],
    );

    const MAX_OPTIONS_LENGTH = 5;
    const displayOptions = React.useMemo(() => {
        if (props.value !== "" && props.value !== undefined)
            return fuse
                .search(props.value)
                .slice(0, MAX_OPTIONS_LENGTH)
                .map((o) => ({ ...o.item, matches: o.matches }));
        if (props.options.length <= MAX_OPTIONS_LENGTH) return props.options;

        return props.options.slice(0, MAX_OPTIONS_LENGTH);
    }, [props.options, fuse, props.value]);

    /* Dropdown state */
    const [dropdownOpen, setDropdownOpen] = React.useState(false);
    const [hoveredOptionID, setHoveredOptionID] =
        React.useState<AutocompleteOptionID | null>(null);

    const hoverNextOption = React.useCallback(() => {
        setHoveredOptionID((currentOptionID) => {
            if (currentOptionID === null)
                return displayOptions[0].value ?? null;
            const currentIndex = displayOptions.findIndex(
                (option) => option.value == currentOptionID,
            );

            if (currentIndex >= displayOptions.length - 1) {
                return displayOptions[0].value;
            }

            return displayOptions[currentIndex + 1].value;
        });
    }, [displayOptions]);

    const hoverPreviousOption = React.useCallback(() => {
        setHoveredOptionID((currentOptionID) => {
            if (currentOptionID === null)
                return displayOptions[displayOptions.length - 1].value ?? null;
            const currentIndex = displayOptions.findIndex(
                (option) => option.value == currentOptionID,
            );

            if (currentIndex <= 0) {
                return displayOptions[displayOptions.length - 1].value;
            }

            return displayOptions[currentIndex - 1].value;
        });
    }, [displayOptions]);

    const resetHoveredOption = React.useCallback(() => {
        setHoveredOptionID(null);
    }, [setHoveredOptionID]);

    /* Input focus management */
    React.useEffect(() => {
        setFocusReceiver(connect(id.current));
        return () => {
            disconnect(id.current);
        };
    }, []);

    const focusEvents = focusReceiver?.receive() ?? [];
    for (const _ of focusEvents) {
        setDropdownOpen(true);
        inputRef.current?.focus();
    }

    /* Helpers */
    const findSelectedOption = React.useCallback(
        (value: AutocompleteOptionID) => {
            const selectedOption = props.options.find(
                (o) => o.value === value || o.label === value,
            );
            return selectedOption;
        },
        [props.options],
    );

    /* Value change handling */
    const changeValueOnType = React.useCallback(
        (newValue: AutocompleteOptionID) => {
            if (props.onChange !== undefined) {
                props.onChange(findSelectedOption(newValue) ?? newValue);
            }
            setDropdownOpen(true);
        },
        [props.onChange, findSelectedOption, setDropdownOpen],
    );
    const changeValueOnClick = React.useCallback(
        (newValue: AutocompleteOptionID) => {
            const selectedOption = findSelectedOption(newValue);
            if (props.onChange !== undefined && selectedOption !== undefined) {
                props.onChange(selectedOption);
            }
            setDropdownOpen(false);
            if (inputRef.current != null) {
                inputRef.current?.focus();
            }
        },
        [props.onChange, findSelectedOption, setDropdownOpen, inputRef],
    );
    const changeValueOnEnter = React.useCallback(
        (newValue: AutocompleteOptionID) => {
            const selectedOption = findSelectedOption(newValue);
            if (props.onChange !== undefined && selectedOption !== undefined) {
                props.onChange(selectedOption);
            }
            setDropdownOpen(false);
            if (inputRef.current !== null) {
                inputRef.current.focus();
            }
        },
        [props.onChange, findSelectedOption, setDropdownOpen, inputRef],
    );

    /*
     * Prevent default browser behavior on keys used for keyboard navigation when dropwdown is open
     */
    const OVERRIDE_KEYS = ["Enter", "ArrowDown", "ArrowUp", "Escape"];
    React.useEffect(() => {
        const handleEvent = (event: KeyboardEvent) => {
            if (
                dropdownOpen &&
                displayOptions.length > 0 &&
                OVERRIDE_KEYS.includes(event.key)
            ) {
                event.preventDefault();
            }
        };
        inputRef.current?.addEventListener("keydown", handleEvent);

        return () => {
            inputRef.current?.removeEventListener("keydown", handleEvent);
        };
    }, [inputRef.current, dropdownOpen]);

    /* Keyboard controls */
    React.useEffect(() => {
        const handleEvent = (event: KeyboardEvent) => {
            if (event.key === "Enter" && hoveredOptionID !== null) {
                const hoveredID = hoveredOptionID;
                resetHoveredOption();
                changeValueOnEnter(hoveredID);
            } else if (event.key === "Enter" && hoveredOptionID === null) {
                setDropdownOpen(false);
                resetHoveredOption();
            } else if (
                event.key === "Tab" ||
                event.key === "Enter" ||
                focusEvents.length > 0
            ) {
                inputRef.current?.click();
                setDropdownOpen(true);
            } else if (event.key === "ArrowDown") {
                setDropdownOpen(true);
                hoverNextOption();
            } else if (event.key === "ArrowUp") {
                setDropdownOpen(true);
                hoverPreviousOption();
            } else if (event.key === "Escape") {
                setDropdownOpen(false);
                resetHoveredOption();
            } else {
                resetHoveredOption();
            }
        };
        inputRef.current?.addEventListener("keyup", handleEvent);

        return () => {
            inputRef.current?.removeEventListener("keyup", handleEvent);
        };
    }, [
        inputRef.current,
        focusEvents,
        setDropdownOpen,
        hoverPreviousOption,
        hoverNextOption,
    ]);

    const invalidStyle: React.CSSProperties = {
        ...props.style,
        borderColor: "#dc3545",
        paddingRight: "calc(1.5em + 0.75rem)",
        backgroundImage: `url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e")`,
        backgroundRepeat: "no-repeat",
        backgroundPosition: "right calc(0.375em + 0.1875rem) center",
        backgroundSize: "calc(0.75em + 0.375rem) calc(0.75em + 0.375rem)",
    };

    const commonJSX = (
        <span className="d-flex flex-column">
            <div
                className={c("position-relative", {
                    "is-invalid": props.isInvalid,
                })}
            >
                <input
                    ref={inputRef}
                    type="text"
                    key={inputID}
                    autoComplete="off"
                    className={c(props.className, "form-control", {
                        "mb-1": props.isInvalid,
                        "form-control-sm": props.size === "sm",
                        "form-control-lg": props.size === "lg",
                    })}
                    style={props.isInvalid ? invalidStyle : props.style}
                    id={inputID}
                    onChange={(e) => changeValueOnType(e.target.value)}
                    onBlur={(e) => {
                        setDropdownOpen(false);
                        resetHoveredOption();
                        if (props.onBlur !== undefined) {
                            const selectedOption = findSelectedOption(
                                e.target.value,
                            );
                            props.onBlur(selectedOption ?? e.target.value);
                        }
                    }}
                    onFocus={() => {
                        resetHoveredOption();
                        setDropdownOpen(true);
                    }}
                    value={props.value}
                    defaultValue={props.defaultValue}
                    placeholder={props.label?.toString() ?? " "}
                />
                {dropdownOpen && (
                    <div
                        ref={container}
                        className="position-absolute border rounded shadow-sm"
                        style={{
                            width: "100%",
                            top: "calc(100% + 5px)",
                            zIndex: 99,
                            background: "white",
                        }}
                        tabIndex={-1}
                    >
                        {/* TODO make style of AutocompleteSelectOption beautiful */}
                        {displayOptions.map((o) => (
                            <AutocompleteSelectOption
                                hovered={o.value === hoveredOptionID}
                                key={o.value}
                                label={highlightMatchedCharacters(
                                    o.matches,
                                    o.label,
                                )}
                                icon={o.markIcon}
                                onClick={() => changeValueOnClick(o.value)}
                            />
                        ))}
                    </div>
                )}
            </div>
            <div className="invalid-feedback">{props.invalidMessage ?? ""}</div>
        </span>
    );

    if (props.floatingLabel) {
        return (
            <div className="form-floating">
                {commonJSX}
                {props.label !== undefined && (
                    <label
                        key={labelID}
                        htmlFor={inputID}
                        className="form-label"
                    >
                        {props.label}
                    </label>
                )}
            </div>
        );
    } else {
        return commonJSX;
    }
};

const AutocompleteSelectOption = (props: AutocompleteSelectOptionProps) => {
    return (
        <button
            tabIndex={-1}
            className={c("dropdown-item", {
                hovered: props.hovered,
            })}
            onClick={props.onClick}
        >
            <div className="d-flex justify-content-between gap-1">
                <span>{props.label}</span>
                {props.icon ? (
                    <i
                        className="material-icons md-18"
                        style={{ color: "rgb(251, 220, 53)", zIndex: 100 }}
                    >
                        {props.icon}
                    </i>
                ) : (
                    ""
                )}
            </div>
        </button>
    );
};

const highlightMatchedCharacters = (
    matches: readonly Fuse.FuseResultMatch[] | undefined,
    string: string,
) => {
    if (matches === undefined) return string;
    let lastIndex = 0;
    let result = [];
    for (const tuple of matches[0].indices) {
        result.push(string.slice(lastIndex, tuple[0]));
        result.push(
            <b key={`(${tuple[0]},${tuple[1] + 1})`}>
                {string.slice(tuple[0], tuple[1] + 1)}
            </b>,
        );
        lastIndex = tuple[1] + 1;
    }
    result.push(string.slice(lastIndex));
    return <span>{result}</span>;
};
