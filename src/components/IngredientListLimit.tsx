import React from "react";
import {
    DragTypes,
    isDragIngredient,
    TListID,
} from "../apps/ingredients-editor/drag";
import { useDrop } from "react-dnd";
interface LimiterProps {
    text: string;
    size: { height: string | number; width: string | number };
    listID: TListID;
    limit: "start" | "end";
}

const IngredientListLimit: React.FC<LimiterProps> = ({
    text,
    size,
    listID,
    limit,
}) => {
    const [, drop] = useDrop({
        accept: DragTypes.Ingredient,
        hover(item, _monitor) {
            if (!isDragIngredient(item)) return;
            item.move({
                type: "limit",
                listID,
                limit,
            });
        },
    });

    return (
        <div ref={drop} style={{ height: size.height, width: size.width }}>
            {text}
        </div>
    );
};

export default IngredientListLimit;
