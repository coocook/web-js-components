import React from "react";
import { DndProvider, useDrag, useDrop, XYCoord } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import "./MealsDishesEditor.css";
import "../util/layout.css";
import * as IO from "../util/io";
import create from "zustand";
import { OrderedMap } from "immutable";
import dayjs from "dayjs";
import LocalizedFormat from "dayjs/plugin/localizedFormat";
dayjs.extend(LocalizedFormat);

import type { TDishID, TMealID } from "../util/types";
import { TProjectPlan, Dish, Meal } from "../apps/meals-dishes-editor/types";
import {
    Direction,
    DragItem,
    DragTypes,
    ElemPath,
    elemPath,
    isDish,
    isDishElemPath,
    isDragItem,
    move,
    pathEquals,
    ProjectPlan,
    resolvePath,
    toDateID,
} from "../apps/meals-dishes-editor/drag";
import { SimplifyTypeOnce } from "../util/utility-types";
import { ShowInputBox, useAsyncComponents } from "./async-components";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useProject } from "../apps/meals-dishes-editor/app-data";
import { toBool } from "../util/promise";
import { APIError } from "../util/api";
import { tryGetErrorMessage } from "../util/error";

const dayjsFromBackendDate = (backendDate: string) =>
    dayjs(backendDate, "YYYY-MM-DD");

const compareDayjs =
    (unit?: dayjs.OpUnitType, direction: "asc" | "desc" = "asc") =>
    (dayA: dayjs.Dayjs, dayB: dayjs.Dayjs): -1 | 0 | 1 => {
        if (dayA.isBefore(dayB, unit)) return direction === "asc" ? -1 : 1;
        if (dayA.isAfter(dayB, unit)) return direction === "asc" ? 1 : -1;
        return 0;
    };

const MealItem = ({ meal }: { meal: Meal }) => {
    const thisPath = elemPath("meal", {
        date: toDateID(meal.date),
        mealId: meal.id,
    });
    const { show } = useMealModal();
    const { showModal, showInputBox, showToast } = useAsyncComponents();

    const project = useProject();
    const queryClient = useQueryClient();

    const moveMutation = useMutation({
        mutationKey: ["move-meal-dish", project.id],
        mutationFn: ({
            sourcePath,
            targetPath,
            direction,
        }: {
            sourcePath: ElemPath;
            targetPath: ElemPath;
            direction: Direction;
        }) => IO.moveMealDish(sourcePath, targetPath, direction),
    });

    const handle = React.useRef<HTMLDivElement | null>(null);
    const ref = React.useRef<HTMLDivElement | null>(null);
    const [, drop] = useDrop({
        accept: [DragTypes.Meal, DragTypes.Dish],
        hover(item, monitor) {
            if (!ref.current) {
                return;
            }
            if (!monitor.isOver({ shallow: true })) return;
            if (!isDragItem(item)) return;

            const sourcePath = item.tmp_source_path;
            const targetPath = thisPath;
            // return if dragMeal and hoverMeal are the same
            if (pathEquals(sourcePath, targetPath)) return;

            const projectPlan = queryClient.getQueryData<ProjectPlan>([
                "project-plan",
                project.id,
            ]);
            if (projectPlan === undefined) return;
            const dragMealOrDish = resolvePath(
                projectPlan,
                item.tmp_source_path,
            );
            const hoverMeal = resolvePath(projectPlan, targetPath);

            if (dragMealOrDish === undefined || hoverMeal === undefined) return;

            const direction = (() => {
                if (isDish(dragMealOrDish)) {
                    if (dragMealOrDish.meal_id === hoverMeal.id)
                        return undefined;
                    if (dragMealOrDish.date.isSame(hoverMeal.date, "days")) {
                        const parentMeal = resolvePath(
                            projectPlan,
                            elemPath("meal", {
                                date: toDateID(dragMealOrDish.date),
                                mealId: dragMealOrDish.meal_id,
                            }),
                        );
                        if (parentMeal === undefined) return undefined;
                        return parentMeal.position < hoverMeal.position
                            ? "over"
                            : "under";
                    } else {
                        return dragMealOrDish.date.isBefore(
                            hoverMeal.date,
                            "days",
                        )
                            ? "over"
                            : "under";
                    }
                }
                if (dragMealOrDish.date.isSame(hoverMeal.date, "days")) {
                    return dragMealOrDish.position < hoverMeal.position
                        ? "under"
                        : "over";
                } else {
                    return dragMealOrDish.date.isBefore(hoverMeal.date, "days")
                        ? "over"
                        : "under";
                }
            })();

            if (direction === undefined) return;

            // Determine rectangle on screen
            const hoverBoundingRect = ref.current?.getBoundingClientRect();

            // Get vertical middle
            const hoverHeight =
                hoverBoundingRect.bottom - hoverBoundingRect.top;
            const hoverMiddleY = hoverHeight / 2;

            // Determine mouse position
            const clientOffset = monitor.getClientOffset();

            // Get pixels to the top
            const hoverClientY =
                (clientOffset as XYCoord).y - hoverBoundingRect.top;

            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%

            // TODO only return if dropzone and draggedItem are neighbours
            // Dragging downwards
            if (
                !isDish(dragMealOrDish) &&
                direction === "under" &&
                hoverClientY < hoverMiddleY - hoverHeight * 0.15
            ) {
                return;
            }

            // Dragging upwards
            if (
                !isDish(dragMealOrDish) &&
                direction === "over" &&
                hoverClientY > hoverMiddleY + hoverHeight * 0.15
            ) {
                return;
            }
            const moved = move(projectPlan, sourcePath, targetPath, direction);
            item.tmp_source_path = moved.newSourcePath;
            item.target_path = moved.newTargetPath;
            item.direction = direction;
            queryClient.setQueryData(
                ["project-plan", project.id],
                moved.projectPlan,
            );
        },
    });

    const [{ isDragging }, drag, preview] = useDrag<
        DragItem,
        any,
        { isDragging: boolean }
    >({
        type: DragTypes.Meal,
        item: {
            start_source_path: thisPath,
            tmp_source_path: thisPath,
            target_path: null,
            direction: null,
        },
        end: (draggedItem, monitor) => {
            if (
                draggedItem.target_path === null ||
                draggedItem.direction === null
            )
                return;
            if (!monitor.didDrop()) return;
            return moveMutation
                .mutateAsync({
                    sourcePath: draggedItem.start_source_path,
                    targetPath: draggedItem.target_path,
                    direction: draggedItem.direction,
                })
                .catch((err: any) => {
                    if (APIError.isAPIError(err) && err.isUniqueMealError()) {
                        const projectPlan =
                            queryClient.getQueryData<ProjectPlan>([
                                "project-plan",
                                project.id,
                            ]);
                        return renameAndMoveMeal(
                            meal,
                            projectPlan!,
                            draggedItem,
                            showInputBox,
                        );
                    } else {
                        return showToast({
                            message:
                                tryGetErrorMessage(err) ??
                                "An error has occurred.",
                            duration: 2000,
                            textColor: "light",
                            bgColor: "danger",
                            closable: true,
                            autoHide: true,
                        });
                    }
                })
                .finally(() => {
                    console.log("Invalidate project-plan query");
                    return queryClient.invalidateQueries([
                        "project-plan",
                        project.id,
                    ]);
                });
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
        isDragging: (monitor) =>
            pathEquals(monitor.getItem()?.tmp_source_path, thisPath),
    });

    preview(drop(ref));
    drag(handle);

    return (
        <div
            ref={ref}
            className={`row w-100 m-0 border-bottom py-3 ${
                isDragging && "opacity-25"
            }`}
            style={{ overflow: "auto" }}
        >
            <div className="col-lg-4 pe-lg-0">
                <div className="p-2 mb-3 mb-lg-0 bg-light border rounded">
                    <div className="d-flex justify-content-lg-between gap-3">
                        <div className="d-flex align-items-center">
                            <div
                                ref={handle}
                                className="user-select-none"
                                style={{ cursor: "move" }}
                            >
                                <i
                                    className="material-icons"
                                    style={{ color: "#dee2e6" }}
                                >
                                    drag_indicator
                                </i>
                            </div>
                            <h5 className="m-0" style={{ hyphens: "auto" }}>
                                {meal.name}
                            </h5>
                        </div>
                        <div className="flex-shrink-0 btn-group" role="group">
                            <button
                                onClick={() =>
                                    show(
                                        {
                                            title: "Edit meal",
                                            buttonText: "Update meal",
                                            date: toDateID(meal.date),
                                            name: meal.name,
                                            comment: meal.comment,
                                        },
                                        (changes) =>
                                            Promise.resolve(changes)
                                                .then((changes) => {
                                                    const projectPlan =
                                                        queryClient.getQueryData<ProjectPlan>(
                                                            [
                                                                "project-plan",
                                                                project.id,
                                                            ],
                                                        );
                                                    let testMeal = {
                                                        ...changes,
                                                        id: meal.id,
                                                    };
                                                    if (
                                                        !projectPlanHasDuplicateMeal(
                                                            projectPlan,
                                                            testMeal,
                                                        )
                                                    ) {
                                                        return changes;
                                                    }
                                                    return showInputBox({
                                                        title: "Meal with same date and name already exists",
                                                        inputs: [
                                                            {
                                                                id: "newName",
                                                                label: "Please provide a new name",
                                                                check: (
                                                                    newName,
                                                                ) => {
                                                                    testMeal.name =
                                                                        newName;
                                                                    return !projectPlanHasDuplicateMeal(
                                                                        projectPlan,
                                                                        testMeal,
                                                                    );
                                                                },
                                                            },
                                                        ],
                                                        yesText: "Rename",
                                                        noText: "Cancel",
                                                    }).then((inputResult) => {
                                                        changes.name =
                                                            inputResult.newName;
                                                        return changes;
                                                    });
                                                })
                                                .then(
                                                    (changedValues) =>
                                                        void IO.updateMeal(
                                                            meal,
                                                            changedValues,
                                                        ),
                                                )
                                                .finally(() =>
                                                    queryClient.invalidateQueries(
                                                        [
                                                            "project-plan",
                                                            project.id,
                                                        ],
                                                    ),
                                                ),
                                    )
                                }
                                className="btn btn-sm btn-outline-secondary"
                            >
                                <i className="material-icons">edit</i>
                            </button>
                            <button
                                onClick={async () => {
                                    if (
                                        meal.dishes.size === 0 ||
                                        (await toBool(
                                            showModal({
                                                title: "Delete",
                                                message: (
                                                    <p>
                                                        Delete{" "}
                                                        <span className="fst-italic">
                                                            {meal.name}
                                                        </span>{" "}
                                                        and its{" "}
                                                        {meal.dishes.size === 1
                                                            ? "dish"
                                                            : meal.dishes.size +
                                                              " dishes"}
                                                        ?
                                                    </p>
                                                ),
                                                yesText: "Delete",
                                                noText: "Cancel",
                                                color: "danger",
                                            }),
                                        ))
                                    ) {
                                        await IO.deleteMeal(meal);
                                        queryClient.invalidateQueries([
                                            "project-plan",
                                            project.id,
                                        ]);
                                    }
                                }}
                                className="btn btn-sm btn-outline-danger"
                            >
                                <i className="material-icons">delete</i>
                            </button>
                        </div>
                    </div>
                    <div className="border-top my-2"></div>
                    <div>
                        {meal.comment && (
                            <div className="mb-2">
                                <i>{meal.comment}</i>
                            </div>
                        )}
                        <div>{`${meal.dishes.size} ${
                            meal.dishes.size === 1 ? "dish" : "dishes"
                        }`}</div>
                    </div>
                </div>
            </div>
            <div className="col-lg-8" style={{ minWidth: 550 }}>
                <DishList dishes={meal.dishes} mealId={meal.id} />
            </div>
        </div>
    );
};

async function renameAndMoveMeal(
    meal: Meal,
    projectPlan: ProjectPlan,
    draggedItem: DragItem,
    showInputBox: ShowInputBox,
) {
    let testMeal = {
        id: draggedItem.start_source_path.mealId,
        date: draggedItem.target_path?.date!,
        name: meal.name,
    };
    return showInputBox({
        title: "Meal with same date and name already exists",
        inputs: [
            {
                id: "newName",
                label: "Please provide a new name",
                check: (newName) => {
                    testMeal.name = newName;
                    return !projectPlanHasDuplicateMeal(projectPlan, testMeal);
                },
            },
        ],
        yesText: "Rename",
        noText: "Cancel",
    })
        .then((inputResult) => {
            return {
                date: draggedItem.start_source_path.date,
                name: inputResult?.newName!,
                comment: meal.comment,
            };
        })
        .then((changedValues) => void IO.updateMeal(meal, changedValues))
        .then(() => {
            if (
                draggedItem.target_path === null ||
                draggedItem.direction === null
            )
                return;

            return void IO.moveMealDish(
                draggedItem.start_source_path,
                draggedItem.target_path,
                draggedItem.direction,
            );
        });
}

const DishItem = ({ dish }: { dish: Dish }) => {
    const queryClient = useQueryClient();
    const project = useProject();

    const moveMutation = useMutation({
        mutationKey: ["move-meal-dish", project.id],
        mutationFn: ({
            sourcePath,
            targetPath,
            direction,
        }: {
            sourcePath: ElemPath;
            targetPath: ElemPath;
            direction: Direction;
        }) => IO.moveMealDish(sourcePath, targetPath, direction),
    });

    const thisPath = elemPath("dish", {
        date: toDateID(dish.date),
        mealId: dish.meal_id,
        dishId: dish.id,
    });
    const handle = React.useRef<HTMLDivElement | null>(null);
    const ref = React.useRef<HTMLAnchorElement | null>(null);
    const [, drop] = useDrop({
        accept: DragTypes.Dish,
        hover(item, monitor) {
            if (!ref.current) {
                return;
            }
            if (
                !isDragItem(item) ||
                !isDishElemPath(item.start_source_path) ||
                !isDishElemPath(item.tmp_source_path)
            )
                return;
            if (!monitor.isOver({ shallow: true })) return;

            const sourcePath = item.tmp_source_path;
            const targetPath = thisPath;
            // return if dragDish and hoverDish are the same
            if (pathEquals(sourcePath, targetPath)) return;

            const projectPlan = queryClient.getQueryData<ProjectPlan>([
                "project-plan",
                project.id,
            ]);
            if (projectPlan === undefined) return;
            const dragDish = resolvePath(projectPlan, item.tmp_source_path);
            const hoverDish = resolvePath(projectPlan, targetPath);

            if (dragDish === undefined || hoverDish === undefined) return;

            const direction = (() => {
                if (dragDish.id === hoverDish.id) return undefined;
                if (dragDish.date === hoverDish.date) {
                    if (dragDish.meal_id === hoverDish.meal_id) {
                        if (dragDish.position === hoverDish.position)
                            return undefined;
                        return dragDish.position < hoverDish.position
                            ? "under"
                            : "over";
                    } else {
                        const dragDishParentMeal = resolvePath(
                            projectPlan,
                            elemPath("meal", {
                                date: toDateID(dragDish.date),
                                mealId: dragDish.meal_id,
                            }),
                        );
                        if (dragDishParentMeal === undefined) return undefined;
                        const hoverDishParentMeal = resolvePath(
                            projectPlan,
                            elemPath("meal", {
                                date: toDateID(hoverDish.date),
                                mealId: hoverDish.meal_id,
                            }),
                        );
                        if (hoverDishParentMeal === undefined) return undefined;
                        return dragDishParentMeal.position <
                            hoverDishParentMeal.position
                            ? "under"
                            : "over";
                    }
                } else {
                    return dragDish.date.isBefore(hoverDish.date, "days")
                        ? "under"
                        : "over";
                }
            })();

            if (direction === undefined) return;

            // Determine rectangle on screen
            const hoverBoundingRect = ref.current?.getBoundingClientRect();

            // Get vertical middle
            const hoverMiddleY =
                (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

            // Determine mouse position
            const clientOffset = monitor.getClientOffset();

            // Get pixels to the top
            const hoverClientY =
                (clientOffset as XYCoord).y - hoverBoundingRect.top;

            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%

            // TODO only return if dropzone and draggedItem are neighbours
            // Dragging downwards
            if (direction === "under" && hoverClientY < hoverMiddleY) {
                return;
            }

            // Dragging upwards
            if (direction === "over" && hoverClientY > hoverMiddleY) {
                return;
            }

            const moved = move(projectPlan, sourcePath, targetPath, direction);
            item.tmp_source_path = moved.newSourcePath;
            item.target_path = moved.newTargetPath;
            item.direction = direction;
            queryClient.setQueryData(
                ["project-plan", project.id],
                moved.projectPlan,
            );
        },
    });

    const [{ isDragging }, drag, preview] = useDrag<
        DragItem,
        any,
        { isDragging: boolean }
    >({
        type: DragTypes.Dish,
        item: {
            start_source_path: thisPath,
            tmp_source_path: thisPath,
            target_path: null,
            direction: null,
        },
        end: async (draggedItem, monitor) => {
            if (
                draggedItem.target_path === null ||
                draggedItem.direction === null
            )
                return;
            if (!monitor.didDrop()) return;
            await moveMutation.mutateAsync({
                sourcePath: draggedItem.start_source_path,
                targetPath: draggedItem.target_path,
                direction: draggedItem.direction,
            });
            queryClient.invalidateQueries(["project-plan", project.id]);
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
        isDragging: (monitor) =>
            pathEquals(monitor.getItem()?.tmp_source_path, thisPath),
    });

    preview(drop(ref));
    drag(handle);

    const { showModal } = useAsyncComponents();

    return (
        <a
            ref={ref}
            href={"dish/" + dish.id}
            draggable={false}
            className="list-group-item list-group-item-action px-2"
            style={
                isDragging
                    ? { borderStyle: "dashed" }
                    : { borderStyle: "solid" }
            }
        >
            <div
                className={`d-flex align-items-center justify-content-between ${
                    isDragging && "opacity-25"
                }`}
            >
                <div className="d-flex">
                    <div
                        ref={handle}
                        className="user-select-none"
                        style={{ cursor: "move" }}
                    >
                        <i
                            className="material-icons"
                            style={{ color: "#dee2e6" }}
                        >
                            drag_indicator
                        </i>
                    </div>
                    <span
                        className="text-end"
                        style={{ display: "inline-block", minWidth: 30 }}
                    >
                        {dish.servings}
                    </span>
                    <span style={{ color: "#dee2e6" }}>|</span>
                    <span>{dish.name}</span>
                    {dish.comment && (
                        <span>
                            <span style={{ color: "#dee2e6" }}>|</span>
                            <i>{dish.comment}</i>
                        </span>
                    )}
                </div>
                <button
                    onClick={(e) => {
                        e.preventDefault();
                        showModal({
                            title: "Delete",
                            message: (
                                <p>
                                    Delete{" "}
                                    <span className="fst-italic">
                                        {dish.name}
                                    </span>
                                    ?
                                </p>
                            ),
                            yesText: "Delete",
                            noText: "Cancel",
                            color: "danger",
                        })
                            .then(() => IO.deleteDish(dish))
                            .then(() =>
                                queryClient.invalidateQueries([
                                    "project-plan",
                                    project.id,
                                ]),
                            );
                    }}
                    className="btn btn-sm btn-outline-danger"
                >
                    <i className="material-icons">delete</i>
                </button>
            </div>
        </a>
    );
};

const DishList = ({
    dishes,
    mealId,
}: {
    dishes: OrderedMap<TDishID, Dish>;
    mealId: TMealID;
}) => {
    const project = useProject();
    const queryClient = useQueryClient();

    const recipes = useQuery({
        queryKey: ["recipes", project.id],
        queryFn: IO.getAllRecipes,
        refetchOnMount: false,
    });

    const [display, setDisplay] = React.useState(false);
    const [dishName, setDishName] = React.useState("");
    const [recipe, setRecipe] = React.useState(0);
    const [servings, setServings] = React.useState(4);

    return (
        <div className="list-group">
            {dishes
                .sortBy((d) => d.position)
                .map((dish) => <DishItem key={`dish_${dish.id}`} dish={dish} />)
                .toList()
                .toArray()}

            <div className="list-group-item w-100 p-0">
                <button
                    style={display ? { display: "none" } : { display: "flex" }}
                    onClick={() => setDisplay(!display)}
                    className="btn p-2 w-100 text-secondary justify-content-center align-items-center"
                >
                    Add Dish
                </button>
                <div
                    style={display ? { display: "block" } : { display: "none" }}
                    className="bg-light p-2"
                    id={`#collapseCreateDishOn${mealId}`}
                >
                    <div className="container my-2 px-3">
                        <div className="row align-items-center">
                            <div className="col p-0">
                                <div className="form-floating">
                                    <input
                                        id="dishNameInput"
                                        onChange={(e) =>
                                            setDishName(e.target.value)
                                        }
                                        value={dishName}
                                        type="text"
                                        placeholder="Name"
                                        className="form-control"
                                    />
                                    <label htmlFor="servingsInput">Name</label>
                                </div>
                            </div>
                            <div className="col-auto">or</div>
                            <div className="col p-0">
                                <div className="form-floating">
                                    <select
                                        id="recipeSelect"
                                        onChange={(e) =>
                                            setRecipe(parseInt(e.target.value))
                                        }
                                        value={recipe}
                                        className="form-select"
                                    >
                                        <option value={0}>
                                            Choose a recipe ...
                                        </option>
                                        {recipes.data != null &&
                                            recipes.data.map((recipe) => {
                                                return (
                                                    <option
                                                        key={recipe.id}
                                                        value={recipe.id}
                                                    >
                                                        {recipe.name}
                                                    </option>
                                                );
                                            })}
                                    </select>
                                    <label htmlFor="recipeSelect">Recipe</label>
                                </div>
                            </div>
                            <div className="col-auto">with</div>
                            <div
                                className="col-auto p-0"
                                style={{ width: 120 }}
                            >
                                <div className="form-floating">
                                    <input
                                        id="servings-input"
                                        onChange={(e) =>
                                            setServings(
                                                parseInt(e.target.value),
                                            )
                                        }
                                        value={servings}
                                        type="number"
                                        className="form-control"
                                        placeholder="Servings"
                                    />
                                    <label htmlFor="servings-input">
                                        Servings
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div className="row">
                            <button
                                onClick={() => setDisplay(!display)}
                                className="btn btn-secondary col-auto me-2"
                            >
                                Close
                            </button>
                            <button
                                onClick={async () => {
                                    if (dishName)
                                        await IO.createDish(
                                            mealId,
                                            dishName,
                                            servings,
                                        );

                                    if (recipe)
                                        await IO.useRecipe(
                                            mealId,
                                            recipe,
                                            servings,
                                        );
                                    queryClient.invalidateQueries([
                                        "project-plan",
                                        project.id,
                                    ]);
                                }}
                                className="btn btn-primary col"
                            >
                                Create dish
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export interface MealsDishesEditorProps {
    initialProjectPlan: TProjectPlan;
    initialRecipes: any;
}

const MealsDishesEditor = ({
    initialProjectPlan,
    initialRecipes,
}: MealsDishesEditorProps) => {
    const project = useProject();
    const queryClient = useQueryClient();
    const projectPlan = useQuery({
        queryKey: ["project-plan", project.id],
        queryFn: async () => {
            const projectPlanResponse = await IO.getProjectPlan();
            return projectPlanResponse.project_plan;
        },
        initialData: initialProjectPlan,
        refetchOnMount: false,
    });

    const recipes = useQuery({
        queryKey: ["recipes", project.id],
        queryFn: IO.getAllRecipes,
        initialData: initialRecipes,
        refetchOnMount: false,
    });
    const { show } = useMealModal();

    let lastDate: string;
    return (
        <DndProvider backend={HTML5Backend}>
            <div>
                {projectPlan.data &&
                    projectPlan.data
                        .sortBy(
                            (_value, key) => dayjsFromBackendDate(key),
                            compareDayjs("day", "asc"),
                        )
                        .map((meals, day) => {
                            lastDate = day;
                            return (
                                <div key={day} className="card mb-4">
                                    <div className="card-header bg-light">
                                        <h3 className="m-0">
                                            {dayjsFromBackendDate(day).format(
                                                "ll",
                                            )}
                                        </h3>
                                    </div>
                                    <div className="card-body p-0">
                                        {meals
                                            .sortBy((value) => value.position)
                                            .map((meal) => {
                                                return (
                                                    <MealItem
                                                        key={`meal_${meal.id}`}
                                                        meal={meal}
                                                    />
                                                );
                                            })
                                            .toList()
                                            .toArray()}
                                        <button
                                            onClick={() =>
                                                show(
                                                    {
                                                        title: `Add meal on ${day}`,
                                                        buttonText: "Add meal",
                                                        date: day,
                                                        name: "",
                                                        comment: "",
                                                    },
                                                    (result) =>
                                                        void IO.createMeal(
                                                            result.name,
                                                            result.comment ??
                                                                "",
                                                            result.date,
                                                        ).then(() =>
                                                            queryClient.invalidateQueries(
                                                                [
                                                                    "project-plan",
                                                                    project.id,
                                                                ],
                                                            ),
                                                        ),
                                                )
                                            }
                                            className="btn createMealOrDishBtn w-100 p-2 text-secondary d-flex justify-content-center align-items-center"
                                        >
                                            <i className="material-icons">
                                                add
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            );
                        })
                        .toList()
                        .toArray()}
                <button
                    onClick={() =>
                        show(
                            {
                                title: "Add meal",
                                buttonText: "Add meal",
                                date: toDateID(dayjs(lastDate).add(1, "day")),
                                name: "",
                                comment: "",
                            },
                            (result) =>
                                void IO.createMeal(
                                    result.name,
                                    result.comment ?? "",
                                    result.date,
                                ).then(() =>
                                    queryClient.invalidateQueries([
                                        "project-plan",
                                        project.id,
                                    ]),
                                ),
                        )
                    }
                    id="toggleAddDayModal"
                    type="button"
                    style={{ border: "2px dashed #dee2e6" }}
                    className="btn bg-light w-100 mb-4"
                >
                    {projectPlan.data?.isEmpty()
                        ? "Add first meal"
                        : "Add meal on another day"}
                </button>

                <MealModal />
            </div>
        </DndProvider>
    );
};

type MealModalConfig = {
    title: string;
    date: string;
    name: string;
    comment: string;
    buttonText: string;
};

export type MealModalResult = SimplifyTypeOnce<
    Pick<MealModalConfig, "date" | "comment"> &
        Required<Pick<MealModalConfig, "name">>
>;

type MealModalStore = {
    open: boolean;
    config: MealModalConfig;
    onSuccess: () => void | Promise<void>;
    onDismiss: () => void | Promise<void>;
    button: HTMLElement | null;
    setConfig: (config: Partial<MealModalConfig>) => void;
    show: (
        config: MealModalConfig,
        onSuccess?: (result: MealModalResult) => void | Promise<void>,
        onDismiss?: () => void | Promise<void>,
    ) => Promise<MealModalResult | undefined>;
    setOpenButton: (button: HTMLElement) => void;
};

const useMealModal = create<MealModalStore>()((set, get) => ({
    open: false,
    config: {
        title: "",
        date: "",
        name: "",
        comment: "",
        buttonText: "",
    },
    onSuccess: () => {},
    onDismiss: () => {},
    setConfig: (config) => {
        set({ config: { ...get().config, ...config } });
    },
    show: async (config, onSuccess, onDismiss) => {
        set({ config });
        return new Promise((resolve) => {
            set({
                onSuccess: () => {
                    const name = get().config.name;
                    if (name === "") {
                        useAsyncComponents.getState().showToast({
                            message:
                                "Name is required. Please provide a non-empty meal name.",
                            bgColor: "danger",
                            textColor: "white",
                            autoHide: true,
                            closable: true,
                        });
                        return;
                    }

                    const result = {
                        name,
                        date: get().config.date,
                        comment: get().config.comment,
                    };
                    if (typeof onSuccess === "function") {
                        onSuccess(result);
                    }
                    set({ open: false });
                    get().button?.click();
                    resolve(result);
                },
                onDismiss: () => {
                    if (typeof onDismiss === "function") {
                        onDismiss();
                    }
                    set({ open: false });
                    get().button?.click();
                    resolve(undefined);
                },
                open: true,
            });
            get().button?.click();
        });
    },
    button: null,
    setOpenButton: (button) => {
        set({ button });
    },
}));

const MealModal = () => {
    const {
        config,
        setConfig,
        setOpenButton,
        onSuccess,
        onDismiss: onError,
    } = useMealModal();

    React.useEffect(() => {
        const button = document.getElementById("showModal-web-js-components");
        if (button !== null) {
            setOpenButton(button);
        }
    }, []);

    return (
        <>
            <button
                id="showModal-web-js-components"
                type="button"
                className="d-none"
                data-bs-toggle="modal"
                data-bs-target="#meal-modal"
            ></button>
            <div
                className="modal fade"
                id="meal-modal"
                data-bs-backdrop="static"
                tabIndex={-1}
                aria-labelledby="modalTitle"
                aria-hidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="modalTitle">
                                {config.title}
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                            ></button>
                        </div>
                        <div id="modalBody" className="modal-body">
                            <div className="mb-2 form-floating">
                                <input
                                    id="dateInput"
                                    type="date"
                                    name="date"
                                    placeholder="YYYY-MM-DD"
                                    required
                                    value={config.date}
                                    onChange={(e) =>
                                        setConfig({ date: e.target.value })
                                    }
                                    className="form-control"
                                />
                                <label
                                    htmlFor="dateInput"
                                    className="form-label"
                                >
                                    Date *
                                </label>
                            </div>
                            <div className="mb-2 form-floating">
                                <input
                                    id="nameInput"
                                    type="text"
                                    name="name"
                                    placeholder="name"
                                    value={config.name ?? ""}
                                    onChange={(e) =>
                                        setConfig({ name: e.target.value })
                                    }
                                    required
                                    className="form-control"
                                />
                                <label
                                    htmlFor="nameInput"
                                    className="form-label"
                                >
                                    Name *
                                </label>
                            </div>
                            <div className="mb-2 form-floating">
                                <input
                                    id="commentInput"
                                    type="text"
                                    name="comment"
                                    placeholder="comment"
                                    value={config.comment ?? ""}
                                    onChange={(e) =>
                                        setConfig({ comment: e.target.value })
                                    }
                                    className="form-control"
                                />
                                <label
                                    htmlFor="commentInput"
                                    className="form-label"
                                >
                                    Comment
                                </label>
                            </div>
                            <div className="text-secondary">* required</div>
                        </div>
                        <div className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-sm btn-secondary"
                                data-bs-dismiss="modal"
                                onClick={onError}
                            >
                                Close
                            </button>
                            <button
                                id="modalConfirmElement"
                                type="button"
                                onClick={onSuccess}
                                className="btn btn-sm btn-primary"
                            >
                                {config.buttonText}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

function projectPlanHasDuplicateMeal(
    projectPlan: TProjectPlan | undefined,
    meal: { id: TMealID; date: string; name: string },
): boolean {
    return (
        projectPlan
            ?.get(meal.date)
            ?.some((m) => m.id !== meal.id && m.name === meal.name) ?? false
    );
}

export default MealsDishesEditor;
