import React, { CSSProperties } from "react";
import { useDrag, useDrop, XYCoord } from "react-dnd";
import {
    DragIngredient,
    DragTypes,
    isDragIngredient,
} from "../apps/ingredients-editor/drag";
import DeleteForever from "mdi-react/DeleteForeverIcon";
import "../util/layout.css";

import type { TIngredient } from "../apps/ingredients-editor/types";
import { useIngredientsEditorStore } from "./IngredientsEditor";
import { useDebounce } from "../util/use-debounce";
import * as IO from "../util/io";
import { Input } from "./input";
import { parseNumber } from "../util/parse";

export interface IngredientProps {
    ingredient: TIngredient;
}

const ingredientStyle: CSSProperties = {
    border: "solid 2px grey",
    padding: "4px",
    borderRadius: "4px",
    width: "100%",
    minHeight: "3rem",
};

type ValueValidationState =
    | { isValid: true }
    | { isValid: false; message: string };

const Ingredient: React.FC<IngredientProps> = ({ ingredient: data }) => {
    const ref = React.useRef<HTMLDivElement>(null);
    const handle = React.useRef<HTMLDivElement>(null);

    const {
        moveUp,
        moveDown,
        moveOverOnTop,
        moveOverBelow,
        changeIngredient,
        deleteIngredient,
        setLastDropTarget,
    } = useIngredientsEditorStore();

    const [valueValidationState, setValueValidationState] =
        React.useState<ValueValidationState>({ isValid: true });
    const setValueValidationError = (message: string) => {
        setValueValidationState({ isValid: false, message });
    };

    const removeValueValidationError = () =>
        setValueValidationState({ isValid: true });

    const [, drop] = useDrop({
        accept: DragTypes.Ingredient,
        hover(item, monitor) {
            if (!ref.current) {
                return;
            }
            if (!isDragIngredient(item)) return;

            const dragIngredient =
                useIngredientsEditorStore.getState().ingredientById(item.id) ??
                item.ingredient;
            const hoverIngredient =
                useIngredientsEditorStore.getState().ingredientById(data.id) ??
                data;

            const sameList: boolean =
                dragIngredient.prepare === hoverIngredient.prepare;
            if (
                sameList &&
                dragIngredient.position === hoverIngredient.position
            ) {
                return;
            }

            const direction =
                dragIngredient.position < hoverIngredient.position
                    ? "downwards"
                    : "upwards";

            // Determine rectangle on screen
            const hoverBoundingRect = ref.current?.getBoundingClientRect();

            // Get vertical middle
            const hoverMiddleY =
                (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

            // Determine mouse position
            const clientOffset = monitor.getClientOffset();

            // Get pixels to the top
            const hoverClientY =
                (clientOffset as XYCoord).y - hoverBoundingRect.top;

            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%

            // Dragging downwards
            if (
                sameList &&
                direction === "downwards" &&
                hoverClientY < hoverMiddleY
            ) {
                return;
            }

            // Dragging upwards
            if (
                sameList &&
                direction === "upwards" &&
                hoverClientY > hoverMiddleY
            ) {
                return;
            }

            if (monitor.canDrop()) {
                item.move({
                    type: "item",
                    direction,
                    listID: hoverIngredient.listID,
                    listItem: {
                        position: hoverIngredient.position,
                        id: hoverIngredient.id,
                    },
                });
            }
        },
        canDrop(item, _monitor) {
            if (!isDragIngredient(item)) return false;
            return item.id !== data.id;
        },
    });

    const [{ isDragging }, drag, preview] = useDrag<
        DragIngredient,
        any,
        { isDragging: boolean }
    >({
        type: DragTypes.Ingredient,
        item: {
            type: DragTypes.Ingredient,
            id: data.id,
            ingredient: data,
            move: (dropTarget) => {
                const ingredient =
                    useIngredientsEditorStore
                        .getState()
                        .ingredientById(data.id) ?? data;

                switch (dropTarget.type) {
                    case "item":
                        {
                            switch (dropTarget.direction) {
                                case "downwards":
                                    {
                                        if (
                                            dropTarget.listID ===
                                            ingredient.listID
                                        ) {
                                            moveDown(
                                                ingredient,
                                                dropTarget.listID,
                                                dropTarget.listItem,
                                            );
                                            break;
                                        }
                                        moveOverOnTop(
                                            ingredient,
                                            dropTarget.listID,
                                            dropTarget.listItem,
                                        );
                                    }
                                    break;
                                case "upwards":
                                    {
                                        if (
                                            dropTarget.listID ===
                                            ingredient.listID
                                        ) {
                                            moveUp(
                                                ingredient,
                                                dropTarget.listID,
                                                dropTarget.listItem,
                                            );
                                            break;
                                        }
                                        moveOverBelow(
                                            ingredient,
                                            dropTarget.listID,
                                            dropTarget.listItem,
                                        );
                                    }
                                    break;
                            }
                        }
                        break;
                    case "limit":
                        {
                            switch (dropTarget.limit) {
                                case "start":
                                    {
                                        if (
                                            dropTarget.listID ===
                                            ingredient.listID
                                        ) {
                                            moveUp(
                                                ingredient,
                                                dropTarget.listID,
                                            );
                                            break;
                                        }
                                        moveOverOnTop(
                                            ingredient,
                                            dropTarget.listID,
                                        );
                                    }
                                    break;
                                case "end":
                                    {
                                        if (
                                            dropTarget.listID ===
                                            ingredient.listID
                                        ) {
                                            moveDown(
                                                ingredient,
                                                dropTarget.listID,
                                            );
                                            break;
                                        }
                                        moveOverBelow(
                                            ingredient,
                                            dropTarget.listID,
                                        );
                                    }
                                    break;
                            }
                        }
                        break;
                }

                setLastDropTarget(dropTarget);
            },
        },
        end: (draggedItem) => {
            const lastDropTarget =
                useIngredientsEditorStore.getState().lastDropTarget;
            const project = useIngredientsEditorStore.getState().project;
            if (lastDropTarget === null) return;
            if (lastDropTarget.type === "item") {
                void IO.moveIngredient(
                    project,
                    draggedItem.id,
                    lastDropTarget.listItem.id,
                    lastDropTarget.direction,
                );
            } else if (lastDropTarget.type === "limit") {
                if (lastDropTarget.limit === "start") {
                    void IO.prependIngredient(
                        project,
                        draggedItem.id,
                        lastDropTarget.listID === "prepared",
                    );
                } else if (lastDropTarget.limit === "end") {
                    void IO.appendIngredient(
                        project,
                        draggedItem.id,
                        lastDropTarget.listID === "prepared",
                    );
                }
            }
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
        isDragging: (monitor) => monitor.getItem()?.id === data.id,
    });

    preview(drop(ref));
    drag(handle);

    const dragStyle: CSSProperties = {
        border: "dashed 2px grey",
        opacity: 0.5,
    };

    const debouncedCommentChange = useDebounce(
        (e: React.ChangeEvent<HTMLInputElement>) =>
            changeIngredient(data.listID, data.id, { comment: e.target.value }),
        1000,
    );

    return (
        <div
            ref={ref}
            className="list-group-item list-group-item-action d-flex p-2 justify-content-between"
        >
            <div className="d-flex gap-2 align-items-start">
                <div
                    ref={handle}
                    style={{ cursor: "move", height: "2.375em" }}
                    className="d-flex flex-row align-items-center"
                >
                    <i className="material-icons" style={{ color: "#dee2e6" }}>
                        drag_indicator
                    </i>
                </div>
                {/* Title */}
                <div
                    style={{ height: "2.375em" }}
                    className="d-flex flex-row align-items-center"
                >
                    <span className="fw-bold">{data.article.name}</span>
                    {data.article.comment && (
                        <span className="text-muted fst-italic fs-6">
                            | {data.article.comment}
                        </span>
                    )}
                </div>
            </div>
            <div className="d-flex gap-2">
                <div className="d-flex flex-row" style={{ width: 250 }}>
                    {/* Value */}
                    <Input
                        className="align-self-start"
                        style={{
                            borderTopRightRadius: 0,
                            borderBottomRightRadius: 0,
                        }}
                        type="number"
                        step="any"
                        min={0}
                        isInvalid={!valueValidationState.isValid}
                        invalidMessage={
                            valueValidationState.isValid
                                ? undefined
                                : valueValidationState.message
                        }
                        defaultValue={data.value}
                        onBlur={(rawValue) => {
                            const parseResult = parseNumber(rawValue);
                            if (parseResult.isOk) {
                                removeValueValidationError();
                                changeIngredient(data.listID, data.id, {
                                    value: parseResult.value,
                                });
                            } else {
                                setValueValidationError(parseResult.message);
                            }
                        }}
                    />
                    {/* Unit */}
                    <select
                        className="form-select align-self-start border-start-0"
                        style={{
                            maxWidth: 120,
                            minWidth: 120,
                            width: 120,
                            borderTopLeftRadius: 0,
                            borderBottomLeftRadius: 0,
                        }}
                        defaultValue={data.current_unit.id}
                        onChange={(e) => {
                            changeIngredient(data.listID, data.id, {
                                current_unit: data.units.find(
                                    (u) =>
                                        u.id ===
                                        Number.parseInt(e.target.value),
                                ),
                            });
                        }}
                    >
                        {data.units.map((unit) => (
                            <option key={unit.id} value={unit.id}>
                                {unit.short_name} ({unit.long_name})
                            </option>
                        ))}
                    </select>
                </div>
                {/* Comment */}
                <div
                    className="input-group align-self-start"
                    style={{ width: 300 }}
                >
                    <input
                        className="form-control"
                        type="text"
                        defaultValue={data.comment}
                        placeholder="comment"
                        onChange={debouncedCommentChange}
                    />
                </div>
                {/* Delete Button */}
                <button
                    className="btn btn-outline-danger btn-sm align-self-start d-flex justify-content-center align-items-center"
                    style={{
                        height: "2.375rem",
                        width: "2.375rem",
                        padding: 0,
                    }}
                    onClick={() => deleteIngredient(data.listID, data.id)}
                >
                    <DeleteForever style={{ width: 24, height: 24 }} />
                </button>
            </div>
        </div>
    );
};

export default Ingredient;
