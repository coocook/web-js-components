import type { ChangeEventHandler } from "react";
import React from "react";
import create from "zustand";

type InputBoxValues = Map<
    string,
    {
        value: string;
        check: (val: string) => boolean;
    }
>;
type AsyncComponentsStore = {
    // private API
    modalProps: ModalProps;
    toastProps: ToastProps;
    inputBoxProps: InputBoxProps;
    inputValues: InputBoxValues;
    handleInputBoxInput: (
        id: string,
        defaultValue?: string,
    ) => ChangeEventHandler<HTMLInputElement | null>;
    modalOpenButton: HTMLElement | null;
    setModalOpenButton: (button: HTMLElement) => void;
    inputBoxOpenButton: HTMLElement | null;
    setInputBoxOpenButton: (button: HTMLElement) => void;

    // public API
    showModal: ShowModal;
    showToast: ShowToast;
    showInputBox: ShowInputBox;
};

export type ShowModal = (options: ModalOptions) => Promise<void>;
export type ShowToast = (options: ToastOptions) => Promise<void>;
export type ShowInputBox = <TID extends string>(
    options: InputOptions<TID>,
) => Promise<InputResult<TID>>;

const closedModalProps = {
    open: false,
    options: {
        title: "",
        message: "",
    },
    handleSuccess: () => {},
    handleCancel: () => {},
};
const closedToastProps = {
    open: false,
    options: {
        message: "",
    },
    handleClose: () => {},
};
const closedInputBoxProps = {
    open: false,
    options: {
        inputs: [],
    },
    invalidInput: null,
    handleSuccess: () => {},
    handleCancel: () => {},
};

export const useAsyncComponents = create<AsyncComponentsStore>()(
    (set, get) => ({
        modalProps: closedModalProps,
        toastProps: closedToastProps,
        inputBoxProps: closedInputBoxProps,
        inputValues: new Map(),

        handleInputBoxInput: (id, defaultValue) => {
            if (defaultValue !== undefined) {
                get().inputValues.set(id, {
                    value: defaultValue,
                    check: get().inputValues.get(id)?.check ?? ((v) => true),
                });
            }
            return (e) => {
                get().inputValues.set(id, {
                    value: e.target?.value,
                    check: get().inputValues.get(id)?.check ?? ((v) => true),
                });
            };
        },

        setModalOpenButton: (button) => set({ modalOpenButton: button }),
        modalOpenButton: null,
        showModal: async (options) =>
            new Promise<void>((resolve, reject) => {
                get().modalOpenButton?.click();
                set({
                    modalProps: {
                        options,
                        open: true,
                        handleSuccess: () => {
                            set({ modalProps: closedModalProps });
                            get().modalOpenButton?.click();
                            resolve();
                        },
                        handleCancel: () => {
                            set({ modalProps: closedModalProps });
                            get().modalOpenButton?.click();
                            reject();
                        },
                    },
                });
            }),
        showToast: async (options) =>
            new Promise<void>((resolve) => {
                set({
                    toastProps: {
                        options,
                        open: true,
                        handleClose: () => {
                            set({ toastProps: closedToastProps });
                            resolve();
                        },
                    },
                });
            }),
        setInputBoxOpenButton: (button) => set({ inputBoxOpenButton: button }),
        inputBoxOpenButton: null,
        showInputBox: (options) =>
            new Promise((resolve, reject) => {
                get().inputBoxOpenButton?.click();
                set({
                    inputValues: new Map(
                        options.inputs.map((inp) => [
                            inp.id,
                            {
                                value: inp.defaultValue ?? "",
                                check: inp.check ?? ((v) => true),
                            },
                        ]),
                    ),
                    inputBoxProps: {
                        options,
                        open: true,
                        invalidInput: null,
                        handleSuccess: () => {
                            let result: { [key: string]: string } = {};
                            const valueMap = get().inputValues;
                            for (const [key, item] of valueMap.entries()) {
                                if (item.check(item.value)) {
                                    result[key] = item.value;
                                } else {
                                    const prevProps = get().inputBoxProps;
                                    set({
                                        inputBoxProps: {
                                            ...prevProps,
                                            invalidInput: {
                                                msg:
                                                    prevProps.options
                                                        .invalidValueMessage ??
                                                    "Please provide a valid input!",
                                            },
                                        },
                                    });
                                    return;
                                }
                            }

                            set({ inputBoxProps: closedInputBoxProps });
                            get().inputBoxOpenButton?.click();
                            resolve(result as any);
                            valueMap.clear();
                        },
                        handleCancel: () => {
                            set({ inputBoxProps: closedInputBoxProps });
                            get().inputBoxOpenButton?.click();
                            reject();
                            get().inputValues.clear();
                        },
                    },
                });
            }),
    }),
);

export const AsyncComponentRenderer = () => {
    return (
        <div id="async-components">
            <Modal />
            <div
                className="position-fixed bottom-0 start-50 translate-middle-x p-3"
                style={{ zIndex: 2055 }}
            >
                <Toast />
            </div>
            <InputBox />
        </div>
    );
};

type ButtonColors =
    | "danger"
    | "warning"
    | "info"
    | "primary"
    | "secondary"
    | "success"
    | "light"
    | "dark";

type ModalOptions = {
    title: string;
    message: string | JSX.Element;
    color?: ButtonColors;
    yesText?: string;
    noText?: string;
};
type ModalProps = {
    open: boolean;
    options: ModalOptions;
    handleSuccess: () => void;
    handleCancel: () => void;
};
const Modal = () => {
    const { modalProps: props, setModalOpenButton } = useAsyncComponents();

    React.useEffect(() => {
        const button = document.getElementById(
            "async-components-modal-show-button",
        );
        if (button !== null) {
            setModalOpenButton(button);
        }
    }, []);

    return (
        <div>
            <button
                id="async-components-modal-show-button"
                type="button"
                className="d-none"
                data-bs-toggle="modal"
                data-bs-target="#async-components-modal"
            ></button>
            <div
                className="modal fade"
                id="async-components-modal"
                data-bs-backdrop="static"
                tabIndex={-1}
                aria-labelledby="modalTitle"
                aria-hidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5
                                className="modal-title"
                                id="async-components-modal-title"
                            >
                                {props.options.title}
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                            ></button>
                        </div>
                        <div
                            id="async-components-modal-body"
                            className="modal-body"
                        >
                            {props.options.message}
                        </div>
                        <div className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-sm btn-secondary"
                                data-bs-dismiss="modal"
                                onClick={props.handleCancel}
                            >
                                {props.options.noText}
                            </button>
                            <button
                                id="async-components-modal-confirm-button"
                                type="button"
                                onClick={props.handleSuccess}
                                className={`btn btn-sm btn-${
                                    props.options.color ?? "primary"
                                }`}
                            >
                                {props.options.yesText}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

type ToastOptions = {
    message: string | JSX.Element;
    bgColor?:
        | "info"
        | "success"
        | "warning"
        | "danger"
        | "primary"
        | "secondary"
        | "light"
        | "dark";
    textColor?:
        | "white"
        | "black"
        | "light"
        | "dark"
        | "primary"
        | "success"
        | "danger";
    closable?: boolean;
    autoHide?: boolean;
    duration?: number;
};

type ToastProps = {
    open: boolean;
    options: ToastOptions;
    handleClose: () => void;
};

const Toast = () => {
    const { toastProps: props } = useAsyncComponents();
    React.useEffect(() => {
        if (props.open && props.options.autoHide) {
            setTimeout(
                () => props.handleClose(),
                props.options?.duration ?? 3000,
            );
        }
    }, [props.open]);

    const color = `text-${props.options?.textColor ?? "black"} bg-${
        props.options?.bgColor ?? "light"
    }`.toString();
    return (
        <div
            className={`toast ${props.open ? "show" : ""} fade ${color}`}
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
        >
            <div className="toast-body d-flex align-items-center gap-2">
                {props.options.message}
                {props.options?.closable && (
                    <button
                        type="button"
                        className="btn-close"
                        data-bs-dismiss="toast"
                        aria-label="Close"
                    ></button>
                )}
            </div>
        </div>
    );
};

type Input<TID extends string> = {
    id: TID;
    label: string;
    defaultValue?: string;
    check?: (val: string) => boolean;
};

type InputOptions<TID extends string> = {
    title?: string;
    color?: ButtonColors;
    inputs: Input<TID>[];
    invalidValueMessage?: string;
    yesText?: string;
    noText?: string;
};

type InputResult<TID extends string> = Record<TID, string>;

type InputBoxProps = {
    open: boolean;
    options: InputOptions<any>;
    invalidInput: { msg: string } | null;
    handleSuccess: () => void;
    handleCancel: () => void;
};

const InputBox = () => {
    const {
        inputBoxProps: props,
        handleInputBoxInput,
        setInputBoxOpenButton,
    } = useAsyncComponents();

    React.useEffect(() => {
        const button = document.getElementById(
            "async-components-input-box-show-button",
        );
        if (button !== null) {
            setInputBoxOpenButton(button);
        }
    }, []);

    return (
        <div>
            <button
                id="async-components-input-box-show-button"
                type="button"
                className="d-none"
                data-bs-toggle="modal"
                data-bs-target="#async-components-input-box"
            ></button>
            <div
                className="modal fade"
                id="async-components-input-box"
                data-bs-backdrop="static"
                tabIndex={-1}
                aria-labelledby="modalTitle"
                aria-hidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            {props.options.title && (
                                <h5
                                    className="modal-title"
                                    id="async-components-input-box-title"
                                >
                                    {props.options.title}
                                </h5>
                            )}
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                            ></button>
                        </div>
                        <div
                            id="async-components-input-box-body"
                            className="modal-body"
                        >
                            {props.invalidInput !== null && (
                                <div className="">{props.invalidInput.msg}</div>
                            )}
                            {(props.options.inputs ?? []).map((input) => (
                                <div className="form-group">
                                    <input
                                        key={input.id}
                                        type="text"
                                        className={`form-control ${
                                            props.invalidInput !== null
                                                ? "is-invalid"
                                                : ""
                                        }`}
                                        placeholder={input.label}
                                        defaultValue={input.defaultValue}
                                        id={input.id}
                                        onChange={handleInputBoxInput(
                                            input.id,
                                            input.defaultValue,
                                        )}
                                    />
                                </div>
                            ))}
                        </div>
                        <div className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-sm btn-secondary btn-outline"
                                data-bs-dismiss="modal"
                                onClick={props.handleCancel}
                            >
                                {props.options.noText ?? "Cancel"}
                            </button>
                            <button
                                id="async-components-input-box-confirm-button"
                                type="button"
                                className={`btn btn-sm btn-${
                                    props.options.color ?? "primary"
                                }`}
                                onClick={props.handleSuccess}
                            >
                                {props.options.yesText ?? "Done"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
