import React from "react";
import Ingredient from "./Ingredient";
import IngredientListLimit from "./IngredientListLimit";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import "./IngredientsEditor.css";
import "../util/layout.css";
import * as IO from "../util/io";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { create } from "zustand";
import { immer } from "zustand/middleware/immer";

import Suspense from "./ReactQuerySuspense";
import { TDropTarget, TListID } from "../apps/ingredients-editor/drag";
import { pipe } from "fp-ts/lib/function";
import { Autocomplete, AutocompleteOption, click } from "./autocomplete";
import { Input } from "./input";
import { nanoid } from "nanoid";
import {
    MinimalIngredient,
    TDishOrRecipe,
    TIngredient,
    TUnit,
} from "../apps/ingredients-editor/types";
import { TIngredientID } from "../util/types";

export interface IngredientsEditorProps {
    project: TDishOrRecipe;
}

type IngredientsEditorStore = {
    project: TDishOrRecipe;
    prepared: TIngredient[];
    notPrepared: TIngredient[];
    lastDropTarget: TDropTarget | null;
    setLastDropTarget: (target: TDropTarget) => void;
    changeIngredient: (
        listID: TListID,
        ingrID: TIngredientID,
        changes: Partial<TIngredient>,
    ) => void;
    deleteIngredient: (listID: TListID, ingrID: TIngredientID) => void;
    populateStore: (
        preparedIngredients: TIngredient[],
        notPreparedIngredients: TIngredient[],
        project: TDishOrRecipe,
    ) => void;
    moveOverOnTop: (
        moved: TIngredient,
        targetListID: TListID,
        target?: { position: number; id: TIngredientID },
    ) => void;
    moveOverBelow: (
        moved: TIngredient,
        targetListID: TListID,
        target?: { position: number; id: TIngredientID },
    ) => void;
    moveUp: (
        moved: TIngredient,
        targetListID: TListID,
        target?: { position: number; id: TIngredientID },
    ) => void;
    moveDown: (
        moved: TIngredient,
        targetListID: TListID,
        target?: { position: number; id: TIngredientID },
    ) => void;
    ingredientById: (ingrID: TIngredientID) => TIngredient | undefined;
};

const changePosition = (amount: number) => (ingredient: TIngredient) => ({
    ...ingredient,
    position: ingredient.position + amount,
});

const setPosition = (position: number) => (ingredient: TIngredient) => ({
    ...ingredient,
    position,
});

function removeFromSortedList(list: TIngredient[], position: number) {
    list.splice(position - 1, 1);
    return list;
}

const prepareFromListID = (listID: TListID) => {
    switch (listID) {
        case "notPrepared":
            return false;
        case "prepared":
            return true;
    }
};

const updatePrepared = (listID: TListID) => (ingredient: TIngredient) => ({
    ...ingredient,
    prepare: prepareFromListID(listID),
    listID,
});

export const useIngredientsEditorStore = create<IngredientsEditorStore>()(
    immer((set, get) => ({
        project: {
            type: "dish",
            id: -1,
            name: "(invalid)",
            specificId: -1,
        },
        prepared: [],
        notPrepared: [],
        lastDropTarget: null,
        setLastDropTarget: (target) => set({ lastDropTarget: target }),
        ingredientById: (ingrID) => {
            const pIngr = get().prepared.find((i) => i.id === ingrID);
            const nPIngr = get().notPrepared.find((i) => i.id === ingrID);
            return pIngr ?? nPIngr;
        },
        changeIngredient: (listID, ingrID, changes) => {
            const idx = get()[listID].findIndex((i) => i.id === ingrID);
            if (changes.value !== undefined && changes.value < 0) {
                return;
            }
            set((state) => {
                if (idx !== undefined) {
                    state[listID][idx] = {
                        ...state[listID][idx],
                        ...changes,
                    };
                }
            });
            void IO.updateIngredient(get().project, get()[listID][idx]);
        },
        deleteIngredient: (listID, ingrID) => {
            set((state) => ({
                [listID]: state[listID].filter((i) => i.id !== ingrID),
            }));
            void IO.deleteIngredient(get().project, ingrID);
        },
        populateStore: (prepared, notPrepared, project) =>
            set({
                prepared: prepared
                    .sort(sortByPosition)
                    .map((i, idx) => setPosition(idx + 1)(i)),
                notPrepared: notPrepared
                    .sort(sortByPosition)
                    .map((i, idx) => setPosition(idx + 1)(i)),
                project,
            }),
        moveOverOnTop: (moved, targetListID, target) =>
            set((state) => {
                if (target === undefined) {
                    const newIngr = pipe(
                        moved,
                        setPosition(1),
                        updatePrepared(targetListID),
                    );
                    state[targetListID] = state[targetListID].map(
                        changePosition(1),
                    );
                    state[targetListID].unshift(newIngr);
                } else {
                    const newIngr = pipe(
                        moved,
                        setPosition(target.position),
                        updatePrepared(targetListID),
                    );

                    state[targetListID] = [
                        ...state[targetListID].slice(0, target.position - 1),
                        newIngr,
                        ...state[targetListID]
                            .slice(target.position - 1)
                            .map(changePosition(1)),
                    ];
                }
                state[moved.listID] = [
                    ...state[moved.listID].slice(0, moved.position - 1),
                    ...state[moved.listID]
                        .slice(moved.position)
                        .map(changePosition(-1)),
                ];
            }),
        moveOverBelow: (moved, targetListID, target) =>
            set((state) => {
                if (target === undefined) {
                    const newIngr = pipe(
                        moved,
                        setPosition(state[targetListID].length + 1),
                        updatePrepared(targetListID),
                    );
                    state[targetListID] = [...state[targetListID], newIngr];
                } else {
                    const newIngr = pipe(
                        moved,
                        setPosition(target.position),
                        updatePrepared(targetListID),
                    );
                    state[targetListID] = [
                        ...state[targetListID].slice(0, target.position),
                        newIngr,
                        ...state[targetListID]
                            .slice(target.position)
                            .map(changePosition(1)),
                    ];
                }
                state[moved.listID] = [
                    ...state[moved.listID].slice(0, moved.position - 1),
                    ...state[moved.listID]
                        .slice(moved.position)
                        .map(changePosition(-1)),
                ];
            }),
        moveUp: (moved, targetListID, target) =>
            set((state) => {
                if (target === undefined) {
                    const newIngr = pipe(
                        moved,
                        setPosition(1),
                        updatePrepared(targetListID),
                    );
                    state[targetListID] = [
                        newIngr,
                        ...state[targetListID]
                            .slice(0, moved.position - 1)
                            .map(changePosition(1)),
                        ...state[targetListID].slice(moved.position),
                    ];
                } else {
                    const newIngr = pipe(
                        moved,
                        setPosition(target.position),
                        updatePrepared(targetListID),
                    );
                    state[targetListID] = [
                        ...state[targetListID].slice(0, target.position - 1),
                        newIngr,
                        ...state[targetListID]
                            .slice(target.position - 1, moved.position - 1)
                            .map(changePosition(1)),
                        ...state[targetListID].slice(moved.position),
                    ];
                }
            }),
        moveDown: (moved, targetListID, target) =>
            set((state) => {
                if (target === undefined) {
                    const newIngr = pipe(
                        moved,
                        setPosition(state[targetListID].length),
                        updatePrepared(targetListID),
                    );
                    state[targetListID] = [
                        ...state[targetListID].slice(0, moved.position - 1),
                        ...state[targetListID]
                            .slice(moved.position)
                            .map(changePosition(-1)),
                        newIngr,
                    ];
                } else {
                    const newIngr = pipe(
                        moved,
                        setPosition(target.position),
                        updatePrepared(targetListID),
                    );
                    state[targetListID] = [
                        ...state[targetListID].slice(0, moved.position - 1),
                        ...state[targetListID]
                            .slice(moved.position, target.position)
                            .map(changePosition(-1)),
                        newIngr,
                        ...state[targetListID].slice(target.position),
                    ];
                }
            }),
    })),
);

const IngredientsEditor: React.FC<IngredientsEditorProps> = ({ project }) => {
    const { prepared, notPrepared, populateStore } =
        useIngredientsEditorStore();

    const ingredientsQuery = useQuery(
        ["ingredients", project],
        () => IO.getAllIngredients(project),
        {
            onSuccess(ingredients) {
                populateStore(
                    ingredients.filter((i) => i.prepare === true),
                    ingredients.filter((i) => i.prepare === false),
                    project,
                );
            },
        },
    );

    return (
        <div className="card my-3">
            <div className="card-header">
                <h3 className="m-0">Ingredients</h3>
            </div>
            <div className="card-body">
                <DndProvider backend={HTML5Backend}>
                    <Suspense
                        query={ingredientsQuery}
                        renderError={<p>Error!</p>}
                        renderLoading={<p>Loading &hellip;</p>}
                    >
                        <div
                            className="flex flex-column align-start"
                            id="ingredients-editor"
                        >
                            <div className="list-header">
                                Prepared Ingredients
                            </div>
                            <div style={{ width: "100%" }} id="prepared-items">
                                <IngredientListLimit
                                    listID="prepared"
                                    limit="start"
                                    size={{ height: "1.5rem", width: "100%" }}
                                    text=""
                                />
                                <div className="list-group w-100">
                                    {prepared.length === 0 && (
                                        <div className="list-group-item py-3 fst-italic">
                                            There are no prepared ingredients
                                            yet. Add an ingredient or drag some
                                            here.
                                        </div>
                                    )}
                                    {prepared.map((ingr_def) => (
                                        <Ingredient
                                            key={ingr_def.id}
                                            ingredient={ingr_def}
                                        />
                                    ))}
                                    <AddIngredientForm
                                        project={project}
                                        prepared={true}
                                    />
                                </div>
                                <IngredientListLimit
                                    listID="prepared"
                                    limit="end"
                                    size={{ height: "1.5rem", width: "100%" }}
                                    text=""
                                />
                            </div>
                            <div className="list-header">
                                Normal Ingredients
                            </div>
                            <div
                                style={{ width: "100%" }}
                                id="not-prepared-items"
                            >
                                <IngredientListLimit
                                    listID="notPrepared"
                                    limit="start"
                                    size={{ height: "1.5rem", width: "100%" }}
                                    text=""
                                />
                                <div className="list-group w-100">
                                    {notPrepared.length === 0 && (
                                        <div className="list-group-item py-3 fst-italic">
                                            There are no normal ingredients yet.
                                            Add an ingredient or drag some here.
                                        </div>
                                    )}
                                    {notPrepared.map((ingr_def) => (
                                        <Ingredient
                                            key={ingr_def.id}
                                            ingredient={ingr_def}
                                        />
                                    ))}
                                    <AddIngredientForm
                                        project={project}
                                        prepared={false}
                                    />
                                </div>
                                <IngredientListLimit
                                    listID="notPrepared"
                                    limit="end"
                                    size={{ height: "1.5rem", width: "100%" }}
                                    text=""
                                />
                            </div>
                        </div>
                    </Suspense>
                </DndProvider>
            </div>
        </div>
    );
};

// If `a` comes before `b` return should be negative
const sortByPosition = (a: TIngredient, b: TIngredient) =>
    a.position - b.position;

const equalsById = (ingr1: TIngredient) => (ingr2: TIngredient) =>
    ingr1.id === ingr2.id;

export default IngredientsEditor;

type AddIngredientFormProps = {
    project: TDishOrRecipe;
    prepared: boolean;
};

const AddIngredientForm = (props: AddIngredientFormProps) => {
    const client = useQueryClient();
    const DEFAULT_VALUE = 1;
    const DEFAULT_COMMENT = "";
    const autocompleteData = useQuery(
        ["autocomplete-data", props.project],
        async () => {
            const [articles, units] = await Promise.all([
                IO.getArticles(props.project),
                IO.getUnits(props.project),
            ]);

            return {
                articles: articles.map((a) => ({
                    label: a.name,
                    value: a.id.toString(),
                    option: a,
                })),
                units: units
                    .map((u) => ({
                        label: `${u.short_name} (${u.long_name})`,
                        value: u.id.toString(),
                        option: u,
                        markIcon: isPreferedUnit(
                            u,
                            article && "id" in article ? article.id : undefined,
                        )
                            ? "star"
                            : undefined,
                    }))
                    .sort(sortUnitOptions),
            };
        },
    );

    const articleInputID = React.useMemo(() => nanoid(10), []);
    const [articleInput, setArticleInput] = React.useState<string>("");
    const [article, setArticle] = React.useState<
        MinimalIngredient["article"] | null
    >(null);
    const [unitInput, setUnitInput] = React.useState("");
    const [unit, setUnit] = React.useState<MinimalIngredient["unit"] | null>(
        null,
    );
    const [value, setValue] =
        React.useState<MinimalIngredient["value"]>(DEFAULT_VALUE);
    const [comment, setComment] =
        React.useState<MinimalIngredient["comment"]>(DEFAULT_COMMENT);

    const hasWrongInput = {
        article: article === null,
        value: value < 0 || Number.isNaN(value),
        unit: unit === null,
        comment: false,
    };

    const formRef = React.useRef<HTMLFormElement>(null);

    const createIngredient = useMutation(
        "create-ingredient",
        async ({
            article,
            value,
            unit,
            comment,
        }: {
            article: MinimalIngredient["article"] | null;
            value: MinimalIngredient["value"];
            unit: MinimalIngredient["unit"] | null;
            comment: MinimalIngredient["comment"];
        }) => {
            if (
                article !== null &&
                unit !== null &&
                Object.values(hasWrongInput).every((b) => !b)
            ) {
                IO.createIngredient(props.project, {
                    article,
                    value,
                    unit,
                    comment,
                    prepare: props.prepared,
                });
            } else {
                return Promise.reject("Invalid input");
            }
        },
        {
            onSuccess: () => {
                client.invalidateQueries(["autocomplete-data", props.project]);
                client.invalidateQueries(["ingredients", props.project]);
                setArticle(null);
                setArticleInput("");
                setValue(DEFAULT_VALUE);
                setUnit(null);
                setUnitInput("");
                setComment(DEFAULT_COMMENT);
                formRef.current?.reset();
                click(articleInputID);
            },
        },
    );

    return (
        <form
            ref={formRef}
            className="list-group-item d-flex gap-2 p-2 bg-light"
            onSubmit={(e) => {
                e.preventDefault();
                createIngredient.mutate({
                    article,
                    value: value,
                    unit,
                    comment,
                });
            }}
        >
            {/* Article */}
            <Autocomplete
                className="align-self-start"
                id={articleInputID}
                label="Article"
                options={autocompleteData.data?.articles ?? []}
                isInvalid={createIngredient.isError && hasWrongInput.article}
                invalidMessage="Article must be provided."
                style={{ width: 400 }}
                value={articleInput}
                onChange={(value) => {
                    if (typeof value === "string") {
                        setArticleInput(value);
                        if (value === "") {
                            setArticle(null);
                        } else {
                            setArticle({ name: value });
                        }
                    } else {
                        setArticleInput(value.label);
                        setArticle({
                            id: value.option.id,
                        });
                    }
                }}
                onBlur={(selected) => {
                    client.setQueryData<typeof autocompleteData.data>(
                        ["autocomplete-data", props.project],
                        (data) => {
                            if (!data) return undefined;
                            return {
                                ...data,
                                units: data.units
                                    .map((u) => {
                                        let markIcon = undefined;
                                        if (
                                            typeof selected !== "string" &&
                                            isPreferedUnit(
                                                u.option,
                                                selected.option.id,
                                            )
                                        ) {
                                            markIcon = "star";
                                        }
                                        return { ...u, markIcon };
                                    })
                                    .sort(sortUnitOptions),
                            };
                        },
                    );
                }}
            />
            <div
                className="d-flex flex-row align-self-start"
                style={{ width: 300 }}
            >
                {/* Value */}
                <Input
                    className="rounded-start"
                    style={{
                        borderTopRightRadius: 0,
                        borderBottomRightRadius: 0,
                    }}
                    label="Value"
                    type="number"
                    size="sm"
                    value={value}
                    isInvalid={createIngredient.isError && hasWrongInput.value}
                    invalidMessage="Value must be provided."
                    defaultValue={DEFAULT_VALUE}
                    onChange={(newValue) =>
                        setValue(Number.parseFloat(newValue))
                    }
                    min={0}
                    step="any"
                />
                {/* Unit */}
                <Autocomplete
                    className="rounded-end"
                    label="Unit"
                    style={{
                        borderTopLeftRadius: 0,
                        borderBottomLeftRadius: 0,
                    }}
                    options={autocompleteData.data?.units ?? []}
                    isInvalid={createIngredient.isError && hasWrongInput.unit}
                    invalidMessage="Unit must be provided."
                    value={unitInput}
                    onChange={(selected) => {
                        if (typeof selected === "string") {
                            setUnitInput(selected);
                            if (selected === "") {
                                setUnit(null);
                            } else {
                                setUnit({ name: selected });
                            }
                        } else {
                            setUnitInput(selected.label);
                            setUnit({
                                id: selected.option.id,
                            });
                        }
                    }}
                />
            </div>
            {/* Comment */}
            <div className="flex-grow-1">
                <Input
                    type="text"
                    label="Comment"
                    size="sm"
                    isInvalid={
                        createIngredient.isError && hasWrongInput.comment
                    }
                    value={comment}
                    defaultValue={DEFAULT_COMMENT}
                    onChange={(newComment) => setComment(newComment)}
                />
            </div>
            {/* Add Button */}
            <button
                className="btn btn-primary text-nowrap align-self-start"
                type="submit"
            >
                Add Ingredient
            </button>
        </form>
    );
};

function isPreferedUnit(unit: TUnit, articleId?: number): boolean {
    return Boolean(
        articleId !== undefined && unit.articles?.includes(articleId),
    );
}

function sortUnitOptions(
    a: AutocompleteOption<TUnit>,
    b: AutocompleteOption<TUnit>,
) {
    if (a.markIcon === b.markIcon) {
        return a.label.localeCompare(b.label);
    }

    return a.markIcon ? -1 : 1;
}
