import React, { InputHTMLAttributes } from "react";
import { nanoid } from "nanoid";
import { c } from "../util/conditional-css-classes";

type InputType = "text" | "number" | "email" | "password";
type InputValue<TType extends InputType> = TType extends "number"
    ? number
    : string;

export type InputProps<TType extends InputType> = Omit<
    InputHTMLAttributes<HTMLInputElement>,
    "type" | "defaultValue" | "value" | "onChange" | "onBlur" | "size"
> & {
    ref?: React.MutableRefObject<HTMLInputElement>;
    size?: "sm" | "md";
    isInvalid?: boolean;
    invalidMessage?: string;
    type: TType;
    label?: string;
    value?: InputValue<TType>;
    defaultValue?: InputValue<TType>;
    onChange?: (
        newValue: string,
        e: React.ChangeEvent<HTMLInputElement>,
    ) => void;
    onBlur?: (newValue: string, e: React.FocusEvent<HTMLInputElement>) => void;
};

export const Input = <TType extends InputType>({
    ref,
    size,
    isInvalid,
    invalidMessage,
    type,
    label,
    value: externalValue,
    defaultValue,
    onChange,
    onBlur,
    className: externalClassName,
    ...rest
}: InputProps<TType>) => {
    const id = React.useMemo(() => nanoid(), []);

    const [value, setValue] = React.useState<string>(
        defaultValue?.toString() ?? (type === "number" ? "0" : ""),
    );

    const handleBlur: React.FocusEventHandler<HTMLInputElement> = (e) => {
        setValue(e.target.value);
        if (onBlur !== undefined) {
            onBlur(e.target.value, e);
        }
    };

    const handleChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setValue(e.target.value);
        if (onChange !== undefined) {
            onChange(e.target.value, e);
        }
    };

    return (
        <>
            <span className="d-flex flex-column">
                <input
                    ref={ref}
                    type={type}
                    className={c(externalClassName, "form-control", {
                        "is-invalid": isInvalid,
                    })}
                    value={externalValue ?? value}
                    id={`${id}-input`}
                    placeholder={label?.toString() ?? " "}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    {...rest}
                />
                <div className="invalid-feedback position-relative bottom-0">
                    {invalidMessage ?? ""}
                </div>
                {label !== undefined && size !== "sm" && (
                    <label htmlFor={`${id}-input`}>{label}</label>
                )}
            </span>
        </>
    );
};
