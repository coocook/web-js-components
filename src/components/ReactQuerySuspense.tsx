import React from "react";
import { UseQueryResult } from "react-query";

type ReactQuerySuspenseProps<TData, TError> = {
    query: UseQueryResult<TData, TError>;
    renderError: JSX.Element;
    renderLoading: JSX.Element;
    children?: JSX.Element;
};
export default function Suspense<TData, TError>({
    children,
    query,
    renderError,
    renderLoading,
}: ReactQuerySuspenseProps<TData, TError>) {
    return (
        <>
            {query.status === "loading"
                ? renderLoading
                : query.status === "error"
                ? renderError
                : children}
        </>
    );
}
