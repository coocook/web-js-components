import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { AsyncComponentRenderer } from "../../components/async-components";
import MealsDishesEditor from "../../components/MealsDishesEditor";

import { SSRData, useBackendDataStore } from "./app-data";

const queryClient = new QueryClient();

function MealsDishesEditorApp({ ssr }: { ssr: SSRData }) {
    const { data, populateStore } = useBackendDataStore();

    if (
        ssr.data === null ||
        ssr.initialProjectPlan === null ||
        ssr.initialRecipes === null
    )
        return <></>;
    if (data === null) {
        populateStore(ssr.data);
    }

    return (
        <QueryClientProvider client={queryClient}>
            <MealsDishesEditor
                {...{
                    initialRecipes: ssr.initialRecipes,
                    initialProjectPlan: ssr.initialProjectPlan,
                }}
            />
            <AsyncComponentRenderer />
        </QueryClientProvider>
    );
}

export default MealsDishesEditorApp;
