import { OrderedMap } from "immutable";
import { create } from "zustand";
import { TMealID, TProject } from "../../util/types";
import { Meal, Recipe } from "./types";

type BackendUrlStore = {
    createMeal: string;
    createDish: string;
    dishFromRecipe: string;
    getProjectPlan: string;
    moveMealDish: string;
    getAllRecipes: string;
};

export type BackendData = {
    project: TProject;
    urls: BackendUrlStore;
};

export type BackendDataStore = {
    data: BackendData | null;
    getData: () => BackendData;
    populateStore: (backendData: BackendData) => void;
    updateUrlStore: (urls: Partial<BackendUrlStore>) => void;
};

export const useBackendDataStore = create<BackendDataStore>()((set, get) => ({
    data: null,
    getData: () => get().data!,
    populateStore: (data) => set({ data }),
    updateUrlStore: (urls) => {
        const data = get().getData();
        set({ data: { ...data, urls: { ...data.urls, ...urls } } });
    },
}));
export const useProject = () =>
    useBackendDataStore((state) => state.getData().project);

export type ProjectPlan = OrderedMap<string, OrderedMap<TMealID, Meal>>;
export type SSRData = {
    data: BackendData | null;
    initialProjectPlan: ProjectPlan | null;
    initialRecipes: Recipe[] | null;
};
