import dayjs from "dayjs";
import { OrderedMap } from "immutable";
import { TDishID, TMealID } from "../../util/types";
import { Dish, Meal } from "./types";

type ItemType = "meal" | "dish";
export type MealListPath = { type: "list"; itemType: "meal"; date: string };
export type MealElemPath = {
    type: "elem";
    itemType: "meal";
    date: string;
    mealId: number;
};
export type DishListPath = {
    type: "list";
    itemType: "dish";
    date: string;
    mealId: number;
};
export type DishElemPath = {
    type: "elem";
    itemType: "dish";
    date: string;
    mealId: number;
    dishId: number;
};
export type ListPath = DishListPath | MealListPath;
export type ElemPath = DishElemPath | MealElemPath;
export type Path = ListPath | ElemPath;

export type ResolvePath<TPath> = TPath extends Path
    ? TPath extends ListPath
        ? TPath extends DishListPath
            ? OrderedMap<number, Dish>
            : OrderedMap<number, Meal>
        : TPath extends DishElemPath
        ? Dish
        : Meal
    : never;

export type DishListPathLike = {
    date: string;
    mealId: number;
};
export function isDishListPathLike(
    itemType: ItemType,
    listPathLike: ListPathLike,
): listPathLike is DishListPathLike {
    return (
        itemType === "dish" &&
        ["date", "mealId"].every((k) => Object.keys(listPathLike).includes(k))
    );
}
export type MealListPathLike = {
    date: string;
};
type ListPathLike = DishListPathLike | MealListPathLike;

export type ListPathFromListPathLike<TListPathLike extends ListPathLike> =
    TListPathLike extends DishListPathLike ? DishListPath : MealListPath;
export function listPath<TListPathLike extends ListPathLike>(
    itemType: ItemType,
    pathLike: TListPathLike,
): ListPathFromListPathLike<TListPathLike> {
    if (isDishListPathLike(itemType, pathLike))
        return {
            type: "list" as const,
            itemType,
            date: pathLike.date,
            mealId: pathLike.mealId,
        } as ListPathFromListPathLike<TListPathLike>;

    return {
        type: "list" as const,
        itemType,
        date: pathLike.date,
    } as ListPathFromListPathLike<TListPathLike>;
}
export type DishElemPathLike = {
    date: string;
    mealId: TMealID;
    dishId: TDishID;
};
export function isDishElemPathLike(
    itemType: ItemType,
    elemPathLike: ElemPathLike,
): elemPathLike is DishElemPathLike {
    return (
        itemType === "dish" &&
        ["date", "mealId", "dishId"].every((k) =>
            Object.keys(elemPathLike).includes(k),
        )
    );
}
export type MealElemPathLike = {
    date: string;
    mealId: TMealID;
};
type ElemPathLike = DishElemPathLike | MealElemPathLike;

export type ElemPathFromElemPathLike<TElemPathLike extends ElemPathLike> =
    TElemPathLike extends DishElemPathLike ? DishElemPath : MealElemPath;

export function elemPath<TElemPathLike extends ElemPathLike>(
    itemType: ItemType,
    pathLike: TElemPathLike,
): ElemPathFromElemPathLike<TElemPathLike> {
    if (isDishElemPathLike(itemType, pathLike))
        return {
            type: "elem" as const,
            itemType,
            date: pathLike.date,
            mealId: pathLike.mealId,
            dishId: pathLike.dishId,
        } as ElemPathFromElemPathLike<TElemPathLike>;

    return {
        type: "elem" as const,
        itemType,
        date: pathLike.date,
        mealId: pathLike.mealId,
    } as ElemPathFromElemPathLike<TElemPathLike>;
}

export type ListPathFromElemPath<TElemPath extends ElemPath> =
    TElemPath extends DishElemPath ? DishListPath : MealListPath;

export function getList<TElemPath extends ElemPath>(
    elemPath: TElemPath,
): ListPathFromElemPath<TElemPath> {
    return listPath(
        elemPath.itemType,
        elemPath,
    ) as ListPathFromElemPath<TElemPath>;
}

export function isMealListPath(path: Path): path is MealListPath {
    return path.type === "list" && path.itemType === "meal";
}

export function isDishListPath(path: Path): path is DishListPath {
    return path.type === "list" && path.itemType === "dish";
}

export function isMealElemPath(path: Path): path is MealElemPath {
    return path.type === "elem" && path.itemType === "meal";
}

export function isDishElemPath(path: Path): path is DishElemPath {
    return path.type === "elem" && path.itemType === "dish";
}

export function pathEquals(pathA: Path, pathB: Path): boolean {
    const common =
        pathA.date === pathB.date &&
        pathA.type === pathB.type &&
        pathA.itemType === pathB.itemType;
    if (!common) {
        return false;
    }

    if (isDishElemPath(pathA) && isDishElemPath(pathB)) {
        return (
            common &&
            pathA.mealId === pathB.mealId &&
            pathA.dishId === pathB.dishId
        );
    } else if (
        (isDishListPath(pathA) && isDishListPath(pathB)) ||
        (isMealElemPath(pathA) && isMealElemPath(pathB))
    ) {
        return common && pathA.mealId === pathB.mealId;
    }

    return true;
}

export function toDateID(dayjsDate: dayjs.Dayjs) {
    return dayjsDate.format("YYYY-MM-DD");
}

export function isDish(dishOrMeal: Dish | Meal): dishOrMeal is Dish {
    return ["meal_id", "prepare_at_meal_id", "servings"].every((k) =>
        Object.keys(dishOrMeal).includes(k),
    );
}

export type ProjectPlan = OrderedMap<string, OrderedMap<TMealID, Meal>>;
/**
 * over -> move before
 * under -> move after
 */
export type Direction = "over" | "under";

export type TargetPath<TSourcePath extends ElemPath> =
    TSourcePath extends DishElemPath
        ? DishElemPath | MealElemPath
        : MealElemPath;

export const resolvePath = <TPath extends Path>(
    projectPlan: ProjectPlan,
    path: TPath,
) => {
    if (isDishListPath(path)) {
        return projectPlan.get(path.date)?.get(path.mealId)
            ?.dishes as ResolvePath<TPath>;
    } else if (isMealListPath(path)) {
        return projectPlan.get(path.date) as ResolvePath<TPath>;
    } else if (isDishElemPath(path)) {
        return projectPlan
            .get(path.date)
            ?.get(path.mealId)
            ?.dishes.get(path.dishId) as ResolvePath<TPath>;
    } else if (isMealElemPath(path)) {
        return projectPlan
            .get(path.date)
            ?.get(path.mealId) as ResolvePath<TPath>;
    }
};

export const updateProjectPlanAtPath = <TPath extends Path>(
    projectPlan: ProjectPlan,
    path: TPath,
    change: ResolvePath<TPath>,
) => {
    if (isMealListPath(path)) {
        return projectPlan.set(path.date, change as ResolvePath<typeof path>);
    }
    let mealList = projectPlan.get(path.date);
    if (mealList !== undefined) {
        if (isMealElemPath(path)) {
            mealList = mealList.set(
                path.mealId,
                change as ResolvePath<typeof path>,
            );
        } else if (isDishListPath(path) || isDishElemPath(path)) {
            let meal = mealList.get(path.mealId);
            if (meal !== undefined) {
                if (isDishListPath(path)) {
                    meal.dishes = change as ResolvePath<typeof path>;
                } else if (isDishElemPath(path)) {
                    meal.dishes = meal.dishes.set(
                        path.dishId,
                        change as ResolvePath<typeof path>,
                    );
                }
                mealList = mealList.set(path.mealId, meal);
            }
        }
        projectPlan = projectPlan.set(path.date, mealList);
    }
    return projectPlan;
};

export const move = <TElemPath extends ElemPath>(
    projectPlan: ProjectPlan,
    sourcePath: TElemPath,
    targetPath: TargetPath<TElemPath>,
    direction: Direction,
) => {
    const sourceElem = resolvePath(projectPlan, sourcePath);
    const targetElem = resolvePath(projectPlan, targetPath);
    const sourceListPath = getList(sourcePath);
    const targetListPath =
        isDishElemPath(sourcePath) &&
        isMealElemPath(targetPath) &&
        targetElem !== undefined
            ? listPath("dish", {
                  date: toDateID(targetElem.date),
                  mealId: targetElem.id,
              })
            : getList(targetPath);
    const sourceList = resolvePath(projectPlan, sourceListPath);
    const targetList = resolvePath(projectPlan, targetListPath);

    if (
        sourceElem === undefined ||
        targetElem === undefined ||
        sourceList === undefined ||
        targetList === undefined
    ) {
        throw new Error("Invalid path");
    }

    sourceElem.date = targetElem.date;
    if (isDish(sourceElem)) {
        if (isDish(targetElem)) {
            sourceElem.position = targetElem.position;
            sourceElem.meal_id = targetElem.meal_id;
        } else {
            if (direction === "over") {
                sourceElem.position = 1;
                sourceElem.meal_id = targetElem.id;
            } else {
                sourceElem.position = targetElem.dishes.size + 1;
                sourceElem.meal_id = targetElem.id;
            }
        }
    } else {
        sourceElem.position = targetElem.position;
    }

    projectPlan = updateProjectPlanAtPath(
        projectPlan,
        sourceListPath,
        sourceList
            .delete(sourceElem.id)
            .sortBy((elem) => elem.position)
            .mapEntries(([key, elem], idx) => [
                key,
                {
                    ...elem,
                    position: idx + 1,
                },
            ]) as any,
    );

    projectPlan = updateProjectPlanAtPath(
        projectPlan,
        targetListPath,
        (pathEquals(sourceListPath, targetListPath)
            ? targetList.delete(sourceElem.id)
            : targetList
        )
            .map((m) => {
                if (
                    (isDishElemPath(sourcePath) &&
                        isDishElemPath(targetPath)) ||
                    (isMealElemPath(sourcePath) && isMealElemPath(targetPath))
                ) {
                    if (direction === "over") {
                        if (m.position >= sourceElem.position) m.position++;
                    } else if (direction === "under") {
                        if (m.position > sourceElem.position) m.position++;
                    }
                } else if (
                    isDishElemPath(sourcePath) &&
                    isMealElemPath(targetPath) &&
                    direction === "over"
                ) {
                    m.position++;
                }
                return m;
            })
            .set(sourceElem.id, sourceElem)
            .sortBy((elem) => elem.position)
            .mapEntries(([key, elem], idx) => [
                key,
                { ...elem, position: idx + 1 },
            ]) as any,
    );

    return {
        projectPlan,
        newSourcePath: (isDishElemPath(sourcePath)
            ? elemPath("dish", {
                  date: targetPath.date,
                  mealId: targetPath.mealId,
                  dishId: sourcePath.dishId,
              })
            : elemPath("meal", {
                  date: targetPath.date,
                  mealId: sourcePath.mealId,
              })) as TElemPath,
        newTargetPath: targetPath,
    };
};

export const DragTypes = {
    Meal: "MEAL",
    Dish: "DISH",
};

export interface DragItem {
    start_source_path: ElemPath;
    tmp_source_path: ElemPath;
    target_path: ElemPath | null;
    direction: Direction | null;
}

export function isDragItem(u: unknown): u is DragItem {
    if (typeof u !== "object") return false;
    if (u == null) return false;
    if (
        !["start_source_path", "tmp_source_path"].every((key) =>
            Object.keys(u).includes(key),
        )
    )
        return false;
    return true;
}
