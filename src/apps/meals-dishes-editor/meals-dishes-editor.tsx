import React from "react";
import { createRoot } from "react-dom/client";
import { z } from "zod";
import { htmlDecoder, ready } from "../../util/io";
import { ZProject } from "../../util/types";
import { BackendData, ProjectPlan, SSRData } from "./app-data";
import MealsDishesEditorApp from "./MealsDishesEditorApp";
import { Recipe, ZProjectPlan, ZRecipe } from "./types";

const readSSRData = (jsonId: string): SSRData | null => {
    const jsonString = document.getElementById(jsonId)?.textContent;
    if (jsonString == null) {
        return null;
    }
    const mealsDishesEditorData: {
        projectId: number;
        projectName: string;
        projectPlan: any;
        recipes: any;
        createMealURL: string;
        createDishURL: string;
        dishFromRecipeURL: string;
        getProjectPlanURL: string;
        moveMealDishURL: string;
        getAllRecipesURL: string;
    } = JSON.parse(htmlDecoder.decode(jsonString));

    let data: BackendData | null = null;
    let initialProjectPlan: ProjectPlan | null = null;
    let initialRecipes: Recipe[] | null = null;

    if (mealsDishesEditorData) {
        data = {
            project: ZProject.parse({
                id: mealsDishesEditorData.projectId,
                name: mealsDishesEditorData.projectName,
            }),
            urls: {
                createMeal: mealsDishesEditorData.createMealURL,
                createDish: mealsDishesEditorData.createDishURL,
                dishFromRecipe: mealsDishesEditorData.dishFromRecipeURL,
                getProjectPlan: mealsDishesEditorData.getProjectPlanURL,
                moveMealDish: mealsDishesEditorData.moveMealDishURL,
                getAllRecipes: mealsDishesEditorData.getAllRecipesURL,
            },
        };
        initialProjectPlan = ZProjectPlan.parse(
            mealsDishesEditorData.projectPlan,
        );
        initialRecipes = z.array(ZRecipe).parse(mealsDishesEditorData.recipes);
    }

    return {
        data,
        initialProjectPlan,
        initialRecipes,
    };
};

ready(() => {
    const containerList = document.getElementsByClassName(
        "meals-dishes-editor",
    );

    for (const container of containerList) {
        const root = createRoot(container);
        const jsonId = container.getAttribute(
            "data-meals-dishes-editor-json-id",
        );
        if (!jsonId) {
            console.error(`Invalid data-meals-dishes-json-id: '${jsonId}'`);
            continue;
        }
        const ssr = readSSRData(jsonId);
        if (!ssr) {
            console.error(`[${jsonId}]: Failed to parse JSON`);
            continue;
        }
        root.render(<MealsDishesEditorApp ssr={ssr} />);
    }
});
