import { OrderedMap } from "immutable";
import { z } from "zod";
import {
    stringify,
    ZDateYMD,
    ZDateYMDTransform,
    ZDishID,
    ZMealID,
} from "../../util/types";

export const ZDish = z.object({
    id: ZDishID,
    name: z.string(),
    comment: z.string(),
    position: z.number(),
    date: ZDateYMDTransform,
    meal_id: ZMealID,
    prepare_at_meal_id: ZMealID.optional().nullable(),
    servings: z.number(),
    delete_url: z.string().url().optional(),
    update_url: z.string().url().optional(),
});

export type Dish = z.infer<typeof ZDish>;

function orderedMapWithNumericKeys<T>(
    record: Record<string, T>,
): OrderedMap<number, T> {
    return OrderedMap(
        Object.entries(record).map((entry) => [parseInt(entry[0]), entry[1]]),
    );
}

export const ZMeal = z.object({
    id: ZMealID,
    name: z.string(),
    comment: z.string(),
    date: ZDateYMDTransform,
    project_id: z.number().nonnegative().int(),
    position: z.number(),
    deletable: z.boolean(),
    dishes: z
        .record(stringify(ZDishID), ZDish)
        .transform(orderedMapWithNumericKeys),
    prepared_dishes: z
        .record(stringify(ZDishID), ZDish)
        .transform(orderedMapWithNumericKeys),
    delete_url: z.string().url().optional(),
    delete_dishes_url: z.string().url().optional(),
    update_url: z.string().url().optional(),
});

export type Meal = z.infer<typeof ZMeal>;

export const ZProjectPlan = z
    .record(
        ZDateYMD,
        z
            .record(stringify(ZMealID), ZMeal)
            .transform(orderedMapWithNumericKeys),
    )
    .transform((map) => OrderedMap(map));

export type TProjectPlan = z.infer<typeof ZProjectPlan>;

export const ZRecipe = z.object({
    id: z.number().int(),
    name: z.string(),
    project_id: z.number().int(),
});

export type Recipe = z.infer<typeof ZRecipe>;
