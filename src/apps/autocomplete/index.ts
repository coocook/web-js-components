import { CCAjaxAutocomplete } from "./components/ajaxAutocomplete";
import { CCAutocomplete } from "./components/autocomplete";
import { CCMultiAutocomplete } from "./components/multiAutocomplete";

customElements.define("cc-autocomplete", CCAutocomplete);
customElements.define("cc-ajax-autocomplete", CCAjaxAutocomplete);
customElements.define("cc-multi-autocomplete", CCMultiAutocomplete);
