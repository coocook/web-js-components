export type SelectOption = {
    id: string | number;
    name: string;
    display?: string;
    mark?: string;
};

export type RenderFunction = (data: any) => SelectOption[];

export type Endpoint = {
    url: URL;
    searchKey: string;
    limitKey: string;
    limit: number;
};

export type InputTypes =
    | "text"
    | "search"
    | "url"
    | "date"
    | "email"
    | "number"
    | "tel";
