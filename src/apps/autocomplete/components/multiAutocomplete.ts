import { SelectOption } from "../types";
import { CCAutocomplete } from "./autocomplete";

export class CCMultiAutocomplete extends CCAutocomplete {
    private internalValue: Set<string>;
    private $display: HTMLDivElement;
    protected name: string;

    constructor() {
        super();

        this.internalValue = new Set();
        this.requiredOnInput = false;
        this.canAdd = true;
        this.$display = document.createElement("div");
        this.$display.className = "mt-2 d-flex gap-2";
        this.shadowRoot!.append(this.$display);
        this.name = this.getAttribute("name") || "multi-autocomplete";
    }

    getFormValue() {
        const values = new FormData();
        for (const value of this.internalValue.values()) {
            values.append(this.name, value);
        }

        return values;
    }

    setInput(option: SelectOption, initial = false) {
        if (initial) {
            this.internalValue = this.getInitialData(String(option.id));
        } else {
            const id = String(option.id).trim();
            if (id.length !== 0) {
                this.internalValue.add(id);
                this.dispatchEvent(
                    new CustomEvent("changed", { detail: "setInput mac" }),
                );
            }
        }

        this.setValidity();

        this.$input.value = "";
        this._internals.setFormValue(this.getFormValue());

        this.updateDisplay();
    }

    private setValidity() {
        if (
            this._attrs.required !== undefined &&
            this.internalValue.size === 0
        ) {
            this._internals.setValidity(
                { valueMissing: true },
                "This field is required",
                this.$input,
            );
        } else {
            this._internals.setValidity(
                this.$input.validity,
                this.$input.validationMessage,
                this.$input,
            );
        }
    }

    private updateDisplay() {
        if (!this.$display) {
            return;
        }

        this.$display.innerHTML = "";

        for (const elem of Array.from(this.internalValue).sort()) {
            const btn = document.createElement("button");
            btn.className = "btn btn-outline-primary btn-sm";
            btn.type = "button";
            btn.innerHTML = `${elem} <i class="material-icons md-18">close</i>`;
            btn.title = `Remove ${elem}`;
            btn.addEventListener("click", (e) => {
                btn.remove();
                this.internalValue.delete(elem);
                this.setValidity();

                this.$input.value = "";
                this._internals.setFormValue(this.getFormValue());
                this.dispatchEvent(
                    new CustomEvent("changed", { detail: "delete mac" }),
                );
            });
            this.$display.append(btn);
        }
    }

    private getInitialData(id: string) {
        const html = document.getElementById(id)?.innerText || "";
        const txt = document.createElement("textarea");
        txt.innerHTML = html;

        try {
            return new Set<string>(JSON.parse(txt.value));
        } catch {
            return new Set<string>();
        }
    }
}
