import { InputTypes, SelectOption } from "../types";

export class CCAutocomplete extends HTMLElement {
    static formAssociated = true;
    static get observedAttributes() {
        return ["required", "value"];
    }

    // HTML references
    protected $input: HTMLInputElement;
    protected $optionContainer: HTMLDivElement;

    protected _attrs: { [key: string]: string } = {};
    protected _internals: ElementInternals;

    protected staticBaseUrl: string;
    protected type: InputTypes = "text";
    protected options: SelectOption[] = [];
    protected canAdd = false;
    protected requiredOnInput = true;

    protected currentFocus = -1;

    constructor() {
        super();

        this._internals = this.attachInternals();
        this.staticBaseUrl = this.getAttribute("static-base-url") || "";
        this.type = (this.getAttribute("type") || "text") as InputTypes;

        // Create a shadow root
        this.attachShadow({
            mode: "open",
            delegatesFocus: true,
        });

        this.$input = document.createElement("input");
        this.$optionContainer = document.createElement("div");

        this.addContent();
    }

    connectedCallback() {
        this.addListener();
        this.setProps();
        this._internals.setValidity(
            this.$input.validity,
            this.$input.validationMessage,
            this.$input,
        );
    }

    attributeChangedCallback(name: string, prev: string, next: string) {
        if (prev !== next) {
            this._attrs[name] = next;
            this.setProps();
            this.$optionContainer.classList.add("d-none");
        }
    }

    formDisabledCallback(disabled: boolean) {
        this.$input.disabled = disabled;
    }
    formResetCallback() {
        this.clear();
    }

    public checkValidity() {
        return this._internals.checkValidity();
    }

    public reportValidity() {
        return this._internals.reportValidity();
    }

    public get validity() {
        return this._internals.validity;
    }

    public get validationMessage() {
        return this._internals.validationMessage;
    }

    private setProps() {
        if (!this.$input) {
            return;
        }

        // loop over the properties and apply them to the input
        for (let prop in this._attrs) {
            switch (prop) {
                case "value":
                    const value = this._attrs[prop];
                    this.setInput({ id: value, name: value }, true);
                    break;
                case "required":
                    if (!this.requiredOnInput) break;
                    const required = this._attrs[prop];
                    this.$input.toggleAttribute(
                        "required",
                        required === "true" || required === "",
                    );
                    break;
            }
        }
    }

    private addContent() {
        // Add icons to the shadow DOM
        const icons = document.createElement("link");
        icons.rel = "stylesheet";
        icons.href = new URL(
            "css/material-design-icons.css",
            this.staticBaseUrl,
        ).toString();
        this.shadowRoot!.append(icons);

        // Add bootstrap to the shadow DOM
        const boostrap = document.createElement("link");
        boostrap.setAttribute("rel", "stylesheet");
        boostrap.setAttribute(
            "href",
            new URL(
                "lib/themed-bootstrap/themed.css",
                this.staticBaseUrl,
            ).toString(),
        );
        this.shadowRoot!.append(boostrap);

        // Apply styles to the shadow DOM
        const styles = document.createElement("link");
        styles.setAttribute("rel", "stylesheet");
        styles.setAttribute(
            "href",
            new URL(
                "lib/coocook-web-components/dist/autocomplete/autocomplete_index.css",
                this.staticBaseUrl,
            ).toString(),
        );
        this.shadowRoot!.append(styles);

        // Add input
        this.$input.className = "form-control";
        this.$input.type = this.type;
        this.$input.placeholder = this.getAttribute("placeholder") || "";

        // For floating labels
        const label = this.getAttribute("label") || "";
        if (label !== "") {
            const inputContainer = document.createElement("div");
            inputContainer.className = "form-floating";
            const labelElement = document.createElement("label");
            labelElement.innerText = label;
            labelElement.htmlFor = "auto-input";
            this.$input.placeholder = label;
            this.$input.id = "auto-input";

            inputContainer.append(this.$input, labelElement);
            this.shadowRoot!.append(inputContainer);
        } else {
            this.shadowRoot!.append(this.$input);
        }

        // Add select list
        const container = document.createElement("div");
        container.className = "autocomplete";
        this.shadowRoot!.append(container);

        this.$optionContainer.className = "items d-none";
        container.append(this.$optionContainer);
        this.setPosition();
    }

    private addListener() {
        this.$input.addEventListener("keydown", (e) => {
            let list = this.$optionContainer!.children;
            if (e.key === "ArrowDown") {
                this.$optionContainer.classList.remove("d-none");
                this.currentFocus++;
                this.addActive(list);
            } else if (e.key === "ArrowUp") {
                this.currentFocus--;
                this.addActive(list);
            } else if (e.key === "Enter") {
                e.preventDefault();
                if (this.currentFocus > -1) {
                    (list[this.currentFocus] as HTMLElement).click();
                } else {
                    if (
                        this.$input.value.length !== 0 &&
                        !/\S/.test(this.$input.value)
                    ) {
                        return;
                    }

                    const value = this.$input.value.trim();
                    if (this.canAdd && value !== "") {
                        this.setInput({ id: value, name: value });
                        this.hideList();
                        return;
                    }

                    if (this._internals.form?.reportValidity()) {
                        this._internals.form?.requestSubmit();
                    }
                }
            } else if (e.key === "Escape") {
                this.hideList();
            }
        });

        this.$input.addEventListener("input", (e) => {
            if (e.target) {
                const value = (e.target as HTMLInputElement).value;
                this.handleInput(value);
            }
        });

        this.$input.addEventListener("click", () => {
            const hidden = this.$optionContainer.classList.toggle("d-none");
            if (hidden) {
                this.resetList();
            }
        });

        this.addEventListener("blur", this.hideList);

        let ticking = false;

        document.addEventListener("scroll", (e) => {
            if (!ticking) {
                window.requestAnimationFrame(() => {
                    this.setPosition();
                    ticking = false;
                });

                ticking = true;
            }
        });
    }

    private resetList() {
        this.currentFocus = -1;
        this.removeActive(this.$optionContainer!.children);
    }

    private hideList() {
        this.$optionContainer.classList.add("d-none");
        this.resetList();
    }

    protected filter(value: string) {
        return this.options.filter((elem) =>
            elem.id === "null" || /\p{Lu}/gu.test(value)
                ? elem.name.includes(value)
                : elem.name.toLowerCase().includes(value.toLowerCase()),
        );
    }

    clear() {
        this.$optionContainer.innerHTML = "";
        const data = this.filter("");
        for (let obj of data) {
            this.createOption(obj, "");
        }
        this.setInput({ id: "", name: "" });
    }

    protected getFormValue(value?: string): string | FormData {
        return value || "";
    }

    protected handleInput(searchValue: string) {
        this._internals.setFormValue(this.getFormValue(searchValue));
        this._internals.setValidity(
            this.$input.validity,
            this.$input.validationMessage,
            this.$input,
        );
        this.currentFocus = -1;

        this.$optionContainer.innerHTML = "";

        const data = this.filter(searchValue);
        for (let obj of data) {
            this.createOption(obj, searchValue);
        }
        this.$optionContainer.classList.remove("d-none");
        this.setPosition();
    }

    protected setInput(option: SelectOption, initial = false) {
        if (!initial) {
            this.dispatchEvent(
                new CustomEvent("changed", { detail: "setInput ac" }),
            );
        }
        this.$input.value = option.name;
        this._internals.setFormValue(this.getFormValue(String(option.id)));
        this._internals.setValidity(
            this.$input.validity,
            this.$input.validationMessage,
            this.$input,
        );
    }

    private removeActive(list: HTMLCollection) {
        for (let elem of list) {
            elem.classList.remove("active");
        }
    }

    private addActive(list: HTMLCollection) {
        if (!list) return;
        this.removeActive(list);
        if (this.currentFocus >= list.length) this.currentFocus = 0;
        if (this.currentFocus < 0) this.currentFocus = list.length - 1;
        list[this.currentFocus].classList.add("active");
        scroll(list[this.currentFocus]);

        function scroll(elem: Element) {
            const container = elem.parentElement!;
            const cBox = container.getBoundingClientRect();
            const eBox = elem.getBoundingClientRect();

            if (cBox.bottom < eBox.bottom) {
                container.scrollBy(0, Math.floor(eBox.bottom - cBox.bottom));
            }

            if (eBox.top < cBox.top) {
                container.scrollBy(0, Math.floor(eBox.top - cBox.top));
            }
        }
    }

    protected createOption(option: SelectOption, search: string) {
        const elem = document.createElement("div");
        elem.className = "option";

        const optionLabel = option.mark
            ? this.markSearch(option.mark, this.getSearchRe(search))
            : option.name;
        const rendered = option.display
            ? option.display.replace("{{label}}", optionLabel)
            : optionLabel;
        elem.innerHTML = `<span>${rendered}</span><input type="hidden" value="${option.id}">`;
        elem.addEventListener("mousedown", (e) => e.preventDefault());
        elem.addEventListener("touchstart", (e) => e.preventDefault());
        elem.addEventListener("click", (e) => {
            this.setInput(option);
            this.hideList();
        });
        this.$optionContainer.append(elem);
    }

    protected markSearch(str: string, searchRe: RegExp | null) {
        if (searchRe === null) {
            return str;
        }

        const matches = new Set<string>();
        let match;

        while (true) {
            match = searchRe.exec(str);
            if (match === null) {
                break;
            }
            matches.add(match[0]);
        }

        for (const match of matches) {
            str = str.replaceAll(match, `<b>${match}</b>`);
        }

        return str;
    }

    protected getSearchRe(search: string) {
        if (search.trim() === "") {
            return null;
        }
        const flags = /\p{Lu}/gu.test(search) ? "gu" : "gui";
        return new RegExp(search.trim(), flags);
    }

    protected setPosition() {
        const iBox = this.$input.getBoundingClientRect();
        const height = document.documentElement.clientHeight;
        const lowerSpace = height - Math.floor(iBox.bottom) - 1;
        const upperSpace = Math.floor(iBox.top);
        const boxHeight = Math.floor((height * 40) / 100) + 1;

        let action: "remove" | "add" = "remove";
        if (lowerSpace < boxHeight) {
            action = "add";
            if (upperSpace < boxHeight) {
                action = "remove";
            }
        }

        this.$optionContainer.classList[action]("top");
    }
}
