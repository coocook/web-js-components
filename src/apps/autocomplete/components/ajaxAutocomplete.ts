import { Endpoint, RenderFunction, SelectOption } from "../types";
import { CCAutocomplete } from "./autocomplete";

export class CCAjaxAutocomplete extends CCAutocomplete {
    private $error: HTMLDivElement;
    private $loader: HTMLDivElement;
    private $display: HTMLDivElement | null = null;

    private abortCtrl = new AbortController();
    private minLength = 0;
    private endpoint: Endpoint;
    public renderFunc: RenderFunction = (data) => [];

    constructor() {
        super();

        // delete all elements with class 'delete-by-autocomplete-js'
        // needed for testing
        for (const node of document.getElementsByClassName(
            "delete-by-autocomplete-js",
        )) {
            node.remove();
        }

        const minLength = parseInt(this.getAttribute("min-length") || "");
        if (Number.isInteger(minLength) && minLength > 0)
            this.minLength = minLength;

        // Create error
        this.$error = document.createElement("div");
        this.$error.innerHTML = "Something went wrong.";
        this.$error.className = "error";

        // Create loader
        this.$loader = document.createElement("div");
        this.$loader.className = "load-wrapper";

        const circle = document.createElement("div");
        circle.className = "loader";
        this.$loader.append(circle);

        // Setup endpoint
        const limit = parseInt(this.getAttribute("limit") || "");
        this.endpoint = {
            url: new URL(
                this.getAttribute("data-url") || window.location.pathname,
            ),
            searchKey: this.getAttribute("search-key") || "search",
            limitKey: this.getAttribute("limit-key") || "limit",
            limit: Number.isInteger(limit) ? limit : 0,
        };

        this.addAditionalContent();
        this.setType();
    }

    addAditionalContent = () => {
        const value = document.createElement("div");
        value.className = "value";
        this.shadowRoot!.append(value);

        this.$display = document.createElement("div");
        value.append(this.$display);

        const clear = document.createElement("button");
        clear.className = "btn-close";
        clear.addEventListener("click", () => this.clear());
        value.append(clear);
    };

    private setType() {
        this.type = "search";
        this.$input.type = "search";
    }

    handleInput(searchValue: string) {
        this.abortCtrl.abort();
        this.abortCtrl = new AbortController();
        this.$optionContainer.innerHTML = "";
        if (searchValue.length <= this.minLength) {
            return;
        }
        this.currentFocus = -1;
        this.$optionContainer.append(this.$loader);
        this.$optionContainer.classList.remove("d-none");
        this.setPosition();

        fetch(this.getUrl(searchValue), { signal: this.abortCtrl.signal })
            .then((res) => {
                if (res.ok) return res.json();
                throw new Error("Network response was not OK.");
            })
            .then((data) => {
                this.$loader.remove();
                const rendered = this.renderFunc(data);
                for (let obj of rendered) {
                    this.createOption(obj, searchValue);
                }
            })
            .catch((err) => {
                if (err.name === "AbortError") return;

                console.error(err);
                this.$loader.remove();
                this.$optionContainer.append(this.$error);
            });
    }

    private getUrl(search: string) {
        const searchParams = this.endpoint.url.searchParams;
        searchParams.delete(this.endpoint.searchKey);
        searchParams.append(this.endpoint.searchKey, search);
        if (
            this.endpoint.limit !== 0 &&
            !searchParams.has(this.endpoint.limitKey)
        ) {
            searchParams.append(
                this.endpoint.limitKey,
                String(this.endpoint.limit),
            );
        }

        return this.endpoint.url;
    }

    setInput(option: SelectOption) {
        if (String(option.id) === "" && this.$input.required) {
            this._internals.setValidity(
                { valueMissing: true },
                "This field is required",
                this.$input,
            );
            this.$input.setCustomValidity("This field is required");
        } else {
            this.$input.setCustomValidity("");
            this._internals.setValidity(
                this.$input.validity,
                this.$input.validationMessage,
                this.$input,
            );
        }
        this.$input.value = "";
        this._internals.setFormValue(String(option.id));

        if (this.$display)
            this.$display.innerHTML = option.display || option.name;
    }
}
