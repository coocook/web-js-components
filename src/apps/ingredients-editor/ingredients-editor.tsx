import React from "react";
import { createRoot } from "react-dom/client";
import { htmlDecoder, ready } from "../../util/io";
import IngredientsEditorApp from "./IngredientsEditorApp";
import { TDishOrRecipe } from "./types";
//import "bootstrap/dist/css/bootstrap.min.css";

const readSSRData = (jsonId: string): TDishOrRecipe | null => {
    const jsonString = document.getElementById(jsonId)?.textContent;
    if (jsonString == null) {
        return null;
    }

    const ingredientsEditorData = JSON.parse(htmlDecoder.decode(jsonString));
    if (!ingredientsEditorData) {
        return null;
    }

    return {
        type: ingredientsEditorData.dish_id ? "dish" : "recipe",
        id: ingredientsEditorData.project_id,
        name: ingredientsEditorData.project_name,
        specificId:
            ingredientsEditorData.dish_id || ingredientsEditorData.recipe_id,
    };
};

ready(() => {
    const containerList = document.getElementsByClassName("ingredients-editor");

    for (const container of containerList) {
        const root = createRoot(container);
        const jsonId = container.getAttribute(
            "data-ingredients-editor-json-id",
        );
        if (!jsonId) {
            console.error(
                `Invalid data-ingredients-editor-json-id: '${jsonId}'`,
            );
            continue;
        }
        const ssr = readSSRData(jsonId);
        if (!ssr) {
            console.error(`[${jsonId}]: Failed to parse JSON`);
            continue;
        }
        root.render(<IngredientsEditorApp ssr={ssr} />);
    }
});
