import React from "react";
import {
    QueryClient,
    QueryClientProvider,
    setLogger as setReactQueryLogger,
} from "react-query";
import IngredientsEditor from "../../components/IngredientsEditor";
import { TDishOrRecipe } from "./types";

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 1000 * 60, // 1min
        },
    },
});

function IngredientsEditorApp({ ssr }: { ssr: TDishOrRecipe }) {
    // TODO: Remove this once we have a proper logging system. For now silence react-query logging to console.
    // Maybe also check for build mode and only silence in production.
    setReactQueryLogger({
        log: (message) => {},
        warn: (message) => {},
        error: (message) => {},
    });
    return (
        <QueryClientProvider client={queryClient}>
            <IngredientsEditor project={ssr} />
        </QueryClientProvider>
    );
}

export default IngredientsEditorApp;
