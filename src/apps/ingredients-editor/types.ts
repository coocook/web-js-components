import { z } from "zod";
import { ZIngredientID, ZPerlBoolean } from "../../util/types";

export const ZArticle = z.object({
    id: z.number().int(),
    name: z.string(),
    comment: z.string(),
});

export const ZBackendUnit = z.object({
    id: z.number().nonnegative().int(),
    short_name: z.string(),
    long_name: z.string(),
});

export const ZUnit = ZBackendUnit.extend({
    articles: z.optional(z.array(z.number())),
});

export const ZBackendIngredient = z.object({
    id: ZIngredientID,
    position: z.number(),
    prepare: ZPerlBoolean,
    article: ZArticle,
    value: z.number().min(0),
    current_unit: ZUnit,
    units: z.array(ZUnit),
    comment: z.string(),
});

export const ZIngredient = ZBackendIngredient.extend({
    prepare: z.boolean(),
    listID: z.enum(["prepared", "notPrepared"]),
});

export const ZDishOrRecipe = z.object({
    type: z.enum(["recipe", "dish"]),
    id: z.number().nonnegative().int(),
    name: z.string(),
    specificId: z.number().nonnegative().int(),
});

export const ZDish = ZDishOrRecipe.extend({
    prepare: z.boolean(),
    position: z.number(),
    listID: z.enum(["prepared", "notPrepared"]),
});

export type TBackendIngredient = z.infer<typeof ZBackendIngredient>;
export type TIngredient = z.infer<typeof ZIngredient>;
export type TArticle = z.infer<typeof ZArticle>;
export type TUnit = z.infer<typeof ZUnit>;
export type TDishOrRecipe = z.infer<typeof ZDishOrRecipe>;
export type TDish = z.infer<typeof ZDish>;

export type MinimalIngredient = {
    article:
        | {
              name: TIngredient["article"]["name"];
          }
        | {
              id: TIngredient["article"]["id"];
          };
    value: TIngredient["value"];
    unit:
        | {
              name: TIngredient["current_unit"]["long_name"];
          }
        | {
              id: TIngredient["current_unit"]["id"];
          };
    comment: TIngredient["comment"];
    prepare: TIngredient["prepare"];
};
