import { TIngredient } from "./types";
import type { TIngredientID } from "../../util/types";

export const DragTypes = {
    Ingredient: "INGREDIENT",
};

export type TListID = "prepared" | "notPrepared";
export type TDropTarget =
    | {
          type: "item";
          listID: TListID;
          listItem: { position: number; id: TIngredientID };
          direction: "downwards" | "upwards";
      }
    | {
          type: "limit";
          listID: TListID;
          limit: "start" | "end";
      };

export interface DragIngredient {
    id: number;
    type: string;
    ingredient: TIngredient;
    move: (dropTarget: TDropTarget) => void;
}

export function isDragIngredient(u: unknown): u is DragIngredient {
    if (typeof u !== "object") return false;
    if (u == null) return false;
    if (
        !["id", "type", "ingredient", "move"].every((key) =>
            Object.keys(u).includes(key),
        )
    )
        return false;
    return true;
}
