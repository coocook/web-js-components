export type SimplifyTypeOnce<T> = T extends object
    ? { [K in keyof T]: T[K] }
    : T;
