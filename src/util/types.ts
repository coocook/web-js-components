import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
dayjs.extend(customParseFormat);
import { z } from "zod";
export const ZArticle = z.object({
    id: z.number(),
    name: z.string(),
    comment: z.string(),
});

export const ZPerlBoolean = z.number().min(0).max(1).int();

export const ZIngredientID = z.number().nonnegative().int();
export const ZDishID = z.number().nonnegative().int();
export const ZMealID = z.number().nonnegative().int();
export const ZProjectID = z.number().nonnegative().int();

export const ZProject = z.object({
    id: ZProjectID,
    name: z.string(),
});

export const ZDateYMD = z.string().regex(/^\d{4}-\d{2}-\d{2}$/);
export const ZDateYMDTransform = ZDateYMD.transform((ymd) =>
    dayjs(ymd, "YYYY-MM-DD"),
);

export const stringify = (schema: z.ZodTypeAny) =>
    z.string().refine((str) => schema.safeParse(JSON.parse(str)).success);

export type TIngredientID = z.infer<typeof ZIngredientID>;
export type TDishID = z.infer<typeof ZDishID>;
export type TMealID = z.infer<typeof ZMealID>;
export type TProject = z.infer<typeof ZProject>;
