import {
    MinimalIngredient,
    TDishOrRecipe,
    TIngredient,
} from "../apps/ingredients-editor/types";
import { backend } from "../constants";
import { TIngredientID } from "./types";

export enum APIMethod {
    Get = "GET",
    Post = "POST",
}

interface APIRoute {
    method: APIMethod;
    path: string;
    requestData?: unknown;
}

interface IngredientsAllAPI extends APIRoute {
    method: APIMethod.Get;
    path: "/ingredients";
}

interface IngredientCreateAPI extends APIRoute {
    method: APIMethod.Post;
    path: "/ingredients/create";
    requestData: { ingredient: MinimalIngredient };
}

interface IngredientUpdateAPI extends APIRoute {
    method: APIMethod.Post;
    path: "/ingredients/update";
    requestData: { ingredient: TIngredient };
}

interface IngredientMoveAPI extends APIRoute {
    method: APIMethod.Post;
    path: "/ingredients/move";
    requestData: {
        sourceId: TIngredientID;
        targetId: TIngredientID;
        direction: "downwards" | "upwards";
    };
}

interface IngredientPrependAPI extends APIRoute {
    method: APIMethod.Post;
    path: "/ingredients/prepend";
    requestData: { ingredientId: TIngredientID; prepare: boolean };
}

interface IngredientAppendAPI extends APIRoute {
    method: APIMethod.Post;
    path: "/ingredients/append";
    requestData: { ingredientId: TIngredientID; prepare: boolean };
}

interface IngredientDeleteAPI extends APIRoute {
    method: APIMethod.Post;
    path: "/ingredients/delete";
    requestData: { id: TIngredientID };
}

interface ArticlesAllAPI extends APIRoute {
    method: APIMethod.Get;
    path: "/articles";
}

interface UnitsAllAPI extends APIRoute {
    method: APIMethod.Get;
    path: "/units";
}

type IngredientAPIPaths =
    | IngredientsAllAPI
    | IngredientCreateAPI
    | IngredientUpdateAPI
    | IngredientMoveAPI
    | IngredientPrependAPI
    | IngredientAppendAPI
    | IngredientDeleteAPI
    | ArticlesAllAPI
    | UnitsAllAPI;

const dishOrRecipeBaseUrl: (project: TDishOrRecipe) => string = (project) => {
    switch (project.type) {
        case "dish":
            return `${backend}/project/${project.id}/${project.name}/dish/${project.specificId}`;
        case "recipe":
            return `${backend}/project/${project.id}/${project.name}/recipe/${project.specificId}`;
    }
};

export async function dishOrRecipeRequest(
    project: TDishOrRecipe,
    { path, method, requestData }: IngredientAPIPaths,
) {
    let requestConfig: RequestInit = {
        method,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    };
    if (requestData) {
        requestConfig = {
            ...requestConfig,
            body: JSON.stringify(requestData),
        };
    }

    const response = await fetch(`${dishOrRecipeBaseUrl(project)}${path}`, {
        method,
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(requestData),
    });
    if (!response.ok) {
        throw new APIError(
            response.status,
            await response.json().then((json) => json.error),
        );
    }

    return (await response.json()) as unknown;
}

export async function get(path: string) {
    const response = await fetch(path, {
        method: APIMethod.Get,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    });

    if (!response.ok) {
        throw new APIError(
            response.status,
            await response.json().then((json) => json.error),
        );
    }

    return (await response.json()) as unknown;
}

export async function post(path: string, requestData: any) {
    const response = await fetch(path, {
        method: APIMethod.Post,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify(requestData),
    });

    if (!response.ok) {
        throw new APIError(
            response.status,
            await response.json().then((json) => json.error),
        );
    }

    return (await response.json()) as unknown;
}

export type BackendErrorData = {
    code: string;
    message: string;
};

export function isBackendErrorData(obj: unknown): obj is BackendErrorData {
    if (typeof obj !== "object") return false;
    if (!Object.keys(obj as {}).some((k) => ["code", "message"].includes(k)))
        return false;
    const maybeBED = obj as { code: unknown; message: unknown };
    return (
        typeof maybeBED.code === "string" &&
        typeof maybeBED.message === "string"
    );
}

export class APIError {
    httpStatus: number;
    error: BackendErrorData;
    constructor(httpStatus: number, error: BackendErrorData) {
        this.httpStatus = httpStatus;
        this.error = error;
    }

    message() {
        return this.error.message;
    }

    isUniqueMealError() {
        return this.error.code === "UNQMEAL";
    }

    static isAPIError(obj: unknown): obj is APIError {
        if (typeof obj !== "object") return false;
        if (
            !Object.keys(obj as {}).some((k) =>
                ["httpStatus", "error"].includes(k),
            )
        )
            return false;
        const maybeAPIError = obj as { httpStatus: unknown; error: unknown };
        return (
            typeof maybeAPIError.httpStatus === "number" &&
            isBackendErrorData(maybeAPIError.error)
        );
    }
}
