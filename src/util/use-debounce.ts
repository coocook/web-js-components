import React from "react";

type Function<Args extends any[], Return> = (...args: Args) => Return;

/**
 *
 * @param f Function to debounce
 * @param timeout in milliseconds that should be waited after calling debounced `f` (default: 3000ms)
 * @returns debounced `f`
 */
export const useDebounce = <Args extends any[]>(
    f: Function<Args, void>,
    timeout?: number,
) => {
    const settedTimeout = React.useRef<number | undefined>(undefined);

    return (...args: Args) => {
        clearTimeout(settedTimeout.current);
        settedTimeout.current = window.setTimeout(
            () => f(...args),
            timeout ?? 3000,
        );
    };
};
