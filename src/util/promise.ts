export const toBool = async (promise: Promise<any>) =>
    promise.then(() => true).catch(() => false);
