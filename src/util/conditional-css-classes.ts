export type ConditionalClasses = Record<string, boolean | undefined>;

export function filterConditionalClasses(
    conditionalClasses: ConditionalClasses,
) {
    return Object.entries(conditionalClasses)
        .filter(([, condition]) => !!condition)
        .map(([className]) => className)
        .join(" ");
}

export function c(...classes: (string | undefined | ConditionalClasses)[]) {
    return classes
        .filter((cl) => cl !== undefined)
        .map((cl) =>
            typeof cl === "string"
                ? cl
                : filterConditionalClasses(cl as ConditionalClasses),
        )
        .join(" ");
}
