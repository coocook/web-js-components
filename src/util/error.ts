import { APIError } from "./api";

/**
 * Try different conventions for storing error messages to retrieve the first one found.
 */
export function tryGetErrorMessage(err: unknown): string | null {
    if (APIError.isAPIError(err)) {
        return err.message();
    } else if (typeof err === "string") {
        return err;
    } else if (typeof err === "object" && err !== null) {
        if (
            Object.keys(err).some((k) => k === "message") &&
            typeof (err as { message: unknown }).message === "string"
        ) {
            return (err as { message: string }).message;
        } else if (
            Object.keys(err).some((k) => k === "msg") &&
            typeof (err as { msg: unknown }).msg === "string"
        ) {
            return (err as { msg: string }).msg;
        }
    }

    return null;
}
