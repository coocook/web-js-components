// TODO: split into io.ts for each app
import { z } from "zod";
import { useBackendDataStore } from "../apps/meals-dishes-editor/app-data";
import { Direction, ElemPath } from "../apps/meals-dishes-editor/drag";
import {
    Dish,
    Meal,
    ZDish,
    ZMeal,
    ZProjectPlan,
    ZRecipe,
} from "../apps/meals-dishes-editor/types";
import {
    MinimalIngredient,
    TBackendIngredient,
    TDishOrRecipe,
    TIngredient,
    ZBackendIngredient,
    ZUnit,
} from "../apps/ingredients-editor/types";
import * as API from "./api";
import { APIMethod } from "./api";
import { TIngredientID, ZArticle, ZIngredientID } from "./types";
import { obToSnake } from "./case-conversion";
import { MealModalResult } from "../components/MealsDishesEditor";

const initialIngredientTransformation: (
    ingredient: TBackendIngredient,
) => TIngredient = (ingredient) => ({
    ...ingredient,
    ...{
        listID: !!ingredient.prepare ? "prepared" : "notPrepared",
        prepare: !!ingredient.prepare,
        units: [ingredient.current_unit, ...ingredient.units],
    },
});

const AllIngredientsValidator = z.array(ZBackendIngredient);

export const getAllIngredients: (
    project: TDishOrRecipe,
) => Promise<TIngredient[]> = async (project) => {
    const ingredients = await API.dishOrRecipeRequest(project, {
        method: APIMethod.Get,
        path: "/ingredients",
    });
    const parsed = AllIngredientsValidator.parse(ingredients);
    return parsed.map(initialIngredientTransformation);
};

const ZIngredientIDResponse = z.object({ id: ZIngredientID });

export const createIngredient: (
    project: TDishOrRecipe,
    ingredient: MinimalIngredient,
) => Promise<void> = async (project, ingredient) => {
    try {
        const success = await API.dishOrRecipeRequest(project, {
            method: APIMethod.Post,
            path: "/ingredients/create",
            requestData: { ingredient },
        });
        z.object({ success: z.number().int().min(1).max(1) }).parse(success);
    } catch (err) {
        throw err;
    }
};

export const updateIngredient: (
    project: TDishOrRecipe,
    ingredient: TIngredient,
) => Promise<number> = async (project, ingredient) => {
    const response = await API.dishOrRecipeRequest(project, {
        method: APIMethod.Post,
        path: "/ingredients/update",
        requestData: { ingredient },
    });
    return ZIngredientIDResponse.parse(response).id;
};

export const moveIngredient = async (
    project: TDishOrRecipe,
    sourceId: TIngredientID,
    targetId: TIngredientID,
    direction: "downwards" | "upwards",
) => {
    await API.dishOrRecipeRequest(project, {
        method: APIMethod.Post,
        path: "/ingredients/move",
        requestData: { sourceId, targetId, direction },
    });
    return true;
};

export const prependIngredient = async (
    project: TDishOrRecipe,
    ingredientId: TIngredientID,
    prepare: boolean,
) => {
    await API.dishOrRecipeRequest(project, {
        method: APIMethod.Post,
        path: "/ingredients/prepend",
        requestData: { ingredientId, prepare },
    });
    return true;
};

export const appendIngredient = async (
    project: TDishOrRecipe,
    ingredientId: TIngredientID,
    prepare: boolean,
) => {
    await API.dishOrRecipeRequest(project, {
        method: APIMethod.Post,
        path: "/ingredients/append",
        requestData: { ingredientId, prepare },
    });
    return true;
};

export const deleteIngredient: (
    project: TDishOrRecipe,
    id: TIngredientID,
) => Promise<number> = async (project, id) => {
    const deletedId = await API.dishOrRecipeRequest(project, {
        method: APIMethod.Post,
        path: "/ingredients/delete",
        requestData: { id },
    });
    return ZIngredientIDResponse.parse(deletedId).id;
};

export const getArticles = async (project: TDishOrRecipe) => {
    const articles = await API.dishOrRecipeRequest(project, {
        method: APIMethod.Get,
        path: `/articles`,
    });
    return z.array(ZArticle).parse(articles);
};

export const getUnits = async (project: TDishOrRecipe) => {
    const units = await API.dishOrRecipeRequest(project, {
        method: APIMethod.Get,
        path: `/units`,
    });
    return z.array(ZUnit).parse(units);
};

const ZMealResponse = z.object({ meal: ZMeal });

export const createMeal: (
    name: string,
    comment: string,
    date: string,
) => Promise<z.infer<typeof ZMealResponse>> = async (name, comment, date) => {
    const createdMeal = await API.post(
        useBackendDataStore.getState().getData().urls.createMeal,
        {
            name: name,
            comment: comment,
            date: date,
        },
    );
    return ZMealResponse.parse(createdMeal);
};

export const updateMeal: (
    meal: Meal,
    updateValues: MealModalResult,
) => Promise<z.infer<typeof ZMeal>> = async (meal, updateValues) => {
    if (meal.update_url === undefined)
        throw new Error(
            `Cannot update meal: meal '${meal.id} - ${meal.name}' is missing field 'update_url'.`,
        );
    const updatedMeal = await API.post(meal.update_url, {
        name: updateValues.name,
        comment: updateValues.comment,
        date: updateValues.date,
    });
    return ZMeal.parse(updatedMeal);
};

const ZSuccess = z.object({ success: z.number().int().min(1).max(1) });

export const deleteMeal = async (meal: Meal) => {
    await API.post(meal.delete_dishes_url ?? "", {});
    const response = await API.post(meal.delete_url ?? "", {});
    ZSuccess.parse(response);
};

const ZDishResponse = z.object({ dish: ZDish });

export const createDish: (
    mealId: number,
    name: string,
    servings: number,
    comment?: string,
) => Promise<z.infer<typeof ZDishResponse>> = async (
    mealId,
    name,
    servings,
    comment,
) => {
    const createdDish = await API.post(
        useBackendDataStore.getState().getData().urls.createDish,
        {
            meal_id: mealId,
            dish: {
                name,
                servings,
                comment,
            },
        },
    );
    return ZDishResponse.parse(createdDish);
};

export const useRecipe: (
    mealId: number,
    recipeId: number,
    servings: number,
    comment?: string,
) => Promise<z.infer<typeof ZDishResponse>> = async (
    mealId,
    recipeId,
    servings,
    comment,
) => {
    const createdDish = await API.post(
        useBackendDataStore.getState().getData().urls.dishFromRecipe,
        {
            meal_id: mealId,
            recipe_id: recipeId,
            servings: servings,
            comment: comment,
        },
    );
    return ZDishResponse.parse({ dish: createdDish });
};

export const deleteDish = async (dish: Dish) => {
    const response = await API.post(dish.delete_url ?? "", {});
    ZSuccess.parse(response);
};

const ZProjectPlanResponse = z.object({
    project_plan: ZProjectPlan,
    get_project_plan_url: z.string().url(),
    move_meal_dish_url: z.string().url(),
});

export const getProjectPlan = async () => {
    const state = useBackendDataStore.getState();
    const response = ZProjectPlanResponse.parse(
        await API.get(state.getData().urls.getProjectPlan),
    );

    state.updateUrlStore({
        getProjectPlan: response.get_project_plan_url,
        moveMealDish: response.move_meal_dish_url,
    });
    return response;
};

export const moveMealDish = async (
    sourcePath: ElemPath,
    targetPath: ElemPath,
    direction: Direction,
) => {
    const state = useBackendDataStore.getState();
    return API.post(state.getData().urls.moveMealDish, {
        source_path: obToSnake(sourcePath),
        target_path: obToSnake(targetPath),
        direction,
    }).then(z.union([ZDish, ZMeal]).parse);
};

const ZAllRecipeResponse = z.object({
    recipes: z.array(ZRecipe),
    get_all_recipes_url: z.string().url(),
});

export const getAllRecipes = async () => {
    const state = useBackendDataStore.getState();
    const response = ZAllRecipeResponse.parse(
        await API.get(state.getData().urls.getAllRecipes),
    );
    state.updateUrlStore({ getAllRecipes: response.get_all_recipes_url });
    return response.recipes;
};

/**
 * Use this to decode HTML encoded JSON data from server
 * We do this because we need to prevent XSS attack via JSON passing
 *
 * this silly namespace is used because there already exists a decodeHtml function in
 * the script of coocook
 */
export const htmlDecoder = {
    decode: (html: string): string => {
        let txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    },
};

export const ready = (fn: () => void) => {
    if (document.readyState !== "loading") {
        fn();
        return;
    }
    document.addEventListener("DOMContentLoaded", fn);
};
