enum ValidationErrorCode {
    MissingValue = 1,
    InvalidFormat,
}

export const Ok = (value: number) => ({
    value,
    isOk: true as true,
    isErr: false as false,
});
export const Err = (code: ValidationErrorCode, message: string) => ({
    code,
    message,
    isOk: false as false,
    isErr: true as true,
});
export type ValidationResult<TType> =
    | {
          isOk: true;
          isErr: false;
          value: TType;
      }
    | {
          isOk: false;
          isErr: true;
          code: ValidationErrorCode;
          message: string;
      };

export function parseNumber(rawNumber: string): ValidationResult<number> {
    if (rawNumber.length == 0) {
        return Err(
            ValidationErrorCode.MissingValue,
            "Number must be provided.",
        );
    }
    const parsed = Number.parseFloat(rawNumber);
    if (Number.isNaN(parsed)) {
        return Err(
            ValidationErrorCode.InvalidFormat,
            "Number is provided in wrong format.",
        );
    }
    return Ok(parsed);
}
