export type CamelCase<T> = {
    [Property in keyof T as SnakeToCamel<Property & string>]: T[Property];
};

export type SnakeToCamel<T extends string> =
    T extends `${infer Foo}_${infer Bar}`
        ? `${Foo}${Capitalize<SnakeToCamel<Bar>>}`
        : T;

export type CamelToSnake<T extends string> =
    T extends `${infer Foo}${infer Bar}`
        ? `${Foo extends Lowercase<Foo>
              ? Foo
              : `_${Lowercase<Foo>}`}${CamelToSnake<Bar>}`
        : T;

export type SnakeCase<T> = {
    [Property in keyof T as CamelToSnake<Property & string>]: T[Property];
};

/*
 * could be useful to ensure no PascalCase -> _pascal_case
export type StringToSnake<T extends string> =
  T extends Uncapitalize<T>
  ? CamelToSnake<T>
  : never;
*/

// functions

// convert to camelCase
export function toCamelCase(s: string): string {
    // no need for capturing group, since the first argument to the replacer
    // function is just the entire match (even with a narrower capturing group)
    return s.replace(/_[a-z]/g, (match) => match[1].toUpperCase());
}

export function obToCamel<T extends object>(ob: T): CamelCase<T> {
    let camelized: any = {};

    for (const [k, v] of Object.entries(ob)) {
        camelized[toCamelCase(k)] = v;
    }

    return camelized;
}

export const arrToCamel = <T extends object>(arr: T[]): CamelCase<T>[] => {
    return arr.map((t) => obToCamel(t));
};

export function ambiguousToCamel<T extends object>(arg: T[]): CamelCase<T>[];
export function ambiguousToCamel<T extends object>(arg: T): CamelCase<T>;
export function ambiguousToCamel(arg: any) {
    if (Array.isArray(arg)) {
        return arrToCamel(arg);
    } else {
        return obToCamel(arg);
    }
}

// convert to snake_case
export function toSnakeCase(s: string): string {
    return s.replace(/[A-Z]/g, (match) => `_${match.toLowerCase()}`);
}

export function obToSnake<T extends object>(ob: T): SnakeCase<T> {
    let snakened: any = {};

    for (const [k, v] of Object.entries(ob)) snakened[toSnakeCase(k)] = v;

    return snakened;
}

export const arrToSnake = <T extends object>(arr: T[]): SnakeCase<T>[] => {
    return arr.map((t) => obToSnake(t));
};

export function ambiguousToSnake<T extends object>(arg: T[]): SnakeCase<T>[];
export function ambiguousToSnake<T extends object>(arg: T): SnakeCase<T>;
export function ambiguousToSnake(arg: any) {
    if (Array.isArray(arg)) {
        return arrToSnake(arg);
    } else {
        return obToSnake(arg);
    }
}
